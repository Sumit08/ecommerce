﻿/*******************************************************************************************
*Created By: Sumit
*Created On: 14 feb 2016
*Modified By: 
*Modified On:
*Purpose: Calling the stored procedure related to the supplier and their related products.
********************************************************************************************/

using EntityModel;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;

namespace DataAccess
{
    public class SupplierDetail
    {
        String strConnString = ConfigurationManager.ConnectionStrings["connectionString"].ConnectionString;

        /// <summary>
        /// Purpose: Calling stored procedure to store supplier detail and returning its Id to the calling method.
        /// </summary>
        public int saveSupplier(User user)
        {
            using (SqlConnection connection = new SqlConnection(strConnString))
            {
                int returnValue;
                SqlCommand myCommand = new SqlCommand();
                myCommand.CommandType = CommandType.StoredProcedure;
                myCommand.CommandText = "InsertSupplierDetail";
                try
                {
                    myCommand.Parameters.Add("@FirstName", SqlDbType.NVarChar).Value = user.firstName;
                    myCommand.Parameters.Add("@LastName", SqlDbType.NVarChar).Value = user.lastName;
                    myCommand.Parameters.Add("@Email", SqlDbType.NVarChar).Value = user.email;
                    myCommand.Parameters.Add("@Password", SqlDbType.NVarChar).Value = user.password;
                    myCommand.Parameters.Add("@MobileNumber", SqlDbType.NVarChar).Value = user.mobileNumber;
                    myCommand.Parameters.Add("@RoleId", SqlDbType.Int).Value = (int)userRole.supplier;//UserRole.getUserRoleID("Supplier");
                    myCommand.Connection = connection;
                    connection.Open();
                    int.TryParse(myCommand.ExecuteScalar().ToString(), out returnValue);
                }
                finally
                {
                    connection.Close();
                    connection.Dispose();
                }
                if (returnValue > 0)
                    return returnValue;
                else
                    return 0;
            }
        }

        /// <summary>
        /// Purpose: Calling stored procedure to store supplier Address.
        /// </summary>
        public int saveAddress(Address address)
        {
            using (SqlConnection connection = new SqlConnection(strConnString))
            {
                int returnValue;
                SqlCommand myCommand = new SqlCommand();
                myCommand.CommandType = CommandType.StoredProcedure;
                myCommand.CommandText = "InsertSupplierAddress";
                try
                {
                    myCommand.Parameters.Add("@Addresses", SqlDbType.NVarChar).Value = address.addresses;
                    myCommand.Parameters.Add("@City", SqlDbType.NVarChar).Value = address.city;
                    myCommand.Parameters.Add("@PostalCode", SqlDbType.NVarChar).Value = address.postalCode;
                    myCommand.Parameters.Add("@UserId", SqlDbType.NVarChar).Value = address.usersId;
                    myCommand.Parameters.Add("@StateId", SqlDbType.NVarChar).Value = address.stateId;
                    myCommand.Connection = connection;
                    connection.Open();
                    returnValue = myCommand.ExecuteNonQuery();
                }
                finally
                {
                    connection.Close();
                    connection.Dispose();
                }
                return returnValue;
            }
        }

        /// <summary>
        /// Purpose: Fetching Product category from database and returning entity type list for binding grid view.
        /// </summary>    
        public List<ProductCategory> getProductCategory()
        {
            SqlConnection connection = new SqlConnection(strConnString);
            string commandString = "Select ProductCategoryId, CategoryName FROM ProductCategory";
            using (SqlCommand command = new SqlCommand(commandString, connection))
            {
                connection.Open();
                using (SqlDataReader sqlReader = command.ExecuteReader())
                {
                    List<ProductCategory> Categories = new List<ProductCategory>();
                    try
                    {
                        while (sqlReader.Read())
                        {
                            Categories.Add(new ProductCategory());
                            Categories[Categories.Count - 1].productCategoryId = Convert.ToInt32((sqlReader["productCategoryId"]).ToString());
                            Categories[Categories.Count - 1].categoryName = sqlReader["categoryName"].ToString();
                        }
                    }
                    finally
                    {
                        command.Dispose();
                        connection.Close();
                    }
                    return Categories;
                }
            }
        }

        /// <summary>
        /// Purpose: Inserting new product by different supplier in Product table and categories in ProductCategoriesRel table.
        /// </summary>
        public int insertProduct(Product product, DataTable CategoryTable)
        {
            using (SqlConnection connection = new SqlConnection(strConnString))
            {
                int returnValue;
                SqlCommand myCommand = new SqlCommand();
                myCommand.CommandType = CommandType.StoredProcedure;
                myCommand.CommandText = "InsertProduct";
                try
                {
                    myCommand.Parameters.Add("@Name", SqlDbType.NVarChar).Value = product.productName;
                    myCommand.Parameters.Add("@UnitPrice", SqlDbType.Money).Value = product.unitPrice;
                    myCommand.Parameters.Add("@UnitWeight", SqlDbType.Float).Value = product.unitWeight;
                    myCommand.Parameters.Add("@InStock", SqlDbType.Int).Value = product.inStock;
                    myCommand.Parameters.Add("@Discount", SqlDbType.Float).Value = product.discount;
                    myCommand.Parameters.Add("@Descriptions", SqlDbType.NVarChar).Value = product.description;
                    myCommand.Parameters.Add("@Picture", SqlDbType.NVarChar).Value = product.picture;
                    myCommand.Parameters.Add("@UserId", SqlDbType.Int).Value = product.usersId;
                    myCommand.Parameters.Add("@ProductCategoryList", SqlDbType.Structured).Value = CategoryTable;

                    myCommand.Connection = connection;
                    connection.Open();
                    int.TryParse(myCommand.ExecuteScalar().ToString(), out returnValue);
                }
                finally
                {
                    connection.Close();
                    connection.Dispose();
                }
                if (returnValue > 0)
                    return returnValue;
                else
                    return 0;
            }

        }

        /// <summary>
        /// Purpose: Fetching Product Name List from database to bind ddl and returning entity type list for binding drop down.
        /// </summary>      
        public List<Product> getProductName(int UserId)
        {
            SqlConnection connection = new SqlConnection(strConnString);
            string commandString = "Select * FROM Product WHERE UsersId=" + UserId;
            using (SqlCommand command = new SqlCommand(commandString, connection))
            {
                connection.Open();
                using (SqlDataReader sqlReader = command.ExecuteReader())
                {
                    List<Product> productList = new List<Product>();
                    try
                    {
                        productList.Add(new Product { productName = "--Select Product--" });
                        while (sqlReader.Read())
                        {
                            productList.Add(new Product
                            {
                                productId = Convert.ToInt32(sqlReader["ProductId"])
                                ,
                                productName = Convert.ToString(sqlReader["Name"])
                            });
                            //productList.Add(new Product());
                            //productList[productList.Count - 1].productName = sqlReader["Name"].ToString();
                            //productList[productList.Count - 1].productId = Convert.ToInt32(sqlReader["ProductId"]);
                        }
                    }
                    finally
                    {
                        command.Dispose();
                        connection.Close();
                    }
                    return productList;
                }
            }
        }

        /// <summary>
        /// Purpose: Fetching Product Detail on selected product on supplier drop down.
        /// </summary>
        public Product getProductDetail(int ProductId)
        {
            SqlConnection connection = new SqlConnection(strConnString);
            string commandString = "Select * FROM Product WHERE ProductId=" + ProductId;

            using (SqlCommand command = new SqlCommand(commandString, connection))
            {
                connection.Open();
                using (SqlDataReader sqlReader = command.ExecuteReader())
                {
                    var product = new Product();
                    try
                    {
                        while (sqlReader.Read())
                        {
                            product.productId = Convert.ToInt32(sqlReader["ProductId"]);
                            product.productName = sqlReader["Name"].ToString();
                            product.unitPrice = Convert.ToSingle(sqlReader["UnitPrice"]);
                            product.unitWeight = Convert.ToSingle(sqlReader["UnitWeight"]);
                            product.inStock = Convert.ToInt32(sqlReader["InStock"]);
                            product.discount = Convert.ToSingle(sqlReader["Discount"]);
                            product.picture = sqlReader["Picture"].ToString();
                            product.description = sqlReader["Descriptions"].ToString();
                        }
                    }
                    finally
                    {
                        command.Dispose();
                        connection.Close();
                    }
                    return product;
                }
            }
        }

        /// <summary>
        /// Purpose: Updating the product detail.
        /// </summary>
        public bool updateProductDetail(Product product, DataTable CategoryTable)
        {
            using (SqlConnection connection = new SqlConnection(strConnString))
            {
                int returnValue;
                SqlCommand myCommand = new SqlCommand();
                myCommand.CommandType = CommandType.StoredProcedure;
                myCommand.CommandText = "UpdateProductDetail";
                try
                {
                    myCommand.Parameters.Add("@Name", SqlDbType.NVarChar).Value = product.productName;
                    myCommand.Parameters.Add("@UnitPrice", SqlDbType.Money).Value = product.unitPrice;
                    myCommand.Parameters.Add("@UnitWeight", SqlDbType.Float).Value = product.unitWeight;
                    myCommand.Parameters.Add("@InStock", SqlDbType.Float).Value = product.inStock;
                    myCommand.Parameters.Add("@Discount", SqlDbType.Float).Value = product.discount;
                    myCommand.Parameters.Add("@Descriptions", SqlDbType.NVarChar).Value = product.description;
                    myCommand.Parameters.Add("@Picture", SqlDbType.NVarChar).Value = product.picture;
                    myCommand.Parameters.Add("@ProductId", SqlDbType.Int).Value = product.productId;
                    myCommand.Parameters.Add("@ProductCategoryList", SqlDbType.Structured).Value = CategoryTable;

                    myCommand.Connection = connection;
                    connection.Open();
                    int.TryParse(myCommand.ExecuteScalar().ToString(), out returnValue);
                }
                finally
                {
                    connection.Close();
                    connection.Dispose();
                }
                if (returnValue > 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }

        /// <summary>
        /// Purpose: Fetiching image detail on the basis of its category to bind listView on home page.
        /// </summary>
        public List<ProductCategory> getProductDetailByCategory()
        {
            SqlConnection connection = new SqlConnection(strConnString);
            string commandString = "SELECT pc.ProductCategoryId, pc.CategoryName,p.ProductId, p.Name, p.UnitPrice, p.UnitWeight, p.InStock, p.Discount, p.Descriptions, p.Picture, p.UsersId FROM ProductCategory pc JOIN ProductCategoryRel pcr ON pc.ProductCategoryId = pcr.ProductCategoryId JOIN Product p ON p.ProductId = pcr.ProductId ORDER BY pc.ProductCategoryId";
            using (SqlCommand myCommand = new SqlCommand(commandString, connection))
            {
                connection.Open();
                using (SqlDataReader sqlReader = myCommand.ExecuteReader())
                {
                    List<ProductCategory> Categories = new List<ProductCategory>();
                    try
                    {
                        while (sqlReader.Read())
                        {
                            int categoryId = Convert.ToInt32((sqlReader["productCategoryId"]).ToString());
                            ProductCategory category;
                            if (Categories.Any(c => c.productCategoryId == categoryId))
                            {
                                category = Categories.FirstOrDefault(c => c.productCategoryId == categoryId);
                            }
                            else
                            {
                                category = new ProductCategory
                                {
                                    productCategoryId = Convert.ToInt32((sqlReader["productCategoryId"]).ToString()),
                                    categoryName = sqlReader["categoryName"].ToString()
                                };
                                Categories.Add(category);
                            }

                            if (category.productList == null)
                            {
                                category.productList = new List<Product>();
                            }

                            Product product = new Product
                            {
                                productName = sqlReader["Name"].ToString(),
                                unitPrice = Convert.ToSingle(sqlReader["UnitPrice"]),
                                picture = sqlReader["Picture"].ToString(),
                                productId = Convert.ToInt32(sqlReader["ProductId"])
                            };
                            category.productList.Add(product);
                        }
                    }
                    finally
                    {
                        myCommand.Dispose();
                        connection.Close();
                    }
                    return Categories;
                }
            }
        }

        /// <summary>
        /// Purpose: Fetching List of Countries.
        /// </summary>
        /// <returns></returns>
        public DataTable fetchCountries()
        {
            SqlConnection connection = new SqlConnection(strConnString);
            string commandString = "SELECT CountryId, CountryName FROM Country";
            DataTable dataTable = new DataTable();
            try
            {
                SqlDataAdapter dataAdapter = new SqlDataAdapter(commandString, connection);
                dataAdapter.Fill(dataTable);
            }
            finally
            {
                connection.Close();
            }

            return dataTable;
        }

        /// <summary>
        /// Purpose: Fetching List of States based on selected country.
        /// </summary>
        /// <returns></returns>
        public DataTable fetchStates(int countryId)
        {
            SqlConnection connection = new SqlConnection(strConnString);
            string commandString = "SELECT StateId, StateName FROM State WHERE CountryId=" + countryId;
            DataTable dataTable = new DataTable();
            try
            {
                SqlDataAdapter dataAdapter = new SqlDataAdapter(commandString, connection);
                dataAdapter.Fill(dataTable);
            }
            finally
            {
                connection.Close();
            }
            return dataTable;
        }

    }
}
