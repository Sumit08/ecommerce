﻿/***********************************************************
*Created By: Sumit
*Created On: 06 feb 2016
*Modified By: 
*Modified On:
*Purpose: Calling the stored procedure.
************************************************************/

using EntityModel;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;

namespace DataAccess
{
    public class UserDetail
    {
        String strConnString = ConfigurationManager.ConnectionStrings["connectionString"].ConnectionString;

        /// <summary>
        /// Purpose: Create new user using Signup.
        /// </summary>
        public bool insertUser(User user)
        {
            using (SqlConnection connect = new SqlConnection(strConnString))
            {
                SqlCommand myCommand = new SqlCommand();
                int returnValue;

                myCommand.CommandType = CommandType.StoredProcedure;
                myCommand.CommandText = "InsertUserSignupDetail";
                try
                {
                    myCommand.Parameters.Add("@Email", SqlDbType.NVarChar).Value = user.email;
                    myCommand.Parameters.Add("@UsersPassword", SqlDbType.NVarChar).Value = user.password;
                    myCommand.Parameters.Add("@RoleId", SqlDbType.Int).Value = (int)userRole.user;
                    myCommand.Connection = connect;
                    connect.Open();
                    returnValue = myCommand.ExecuteNonQuery();
                }
                finally
                {
                    connect.Close();
                    connect.Dispose();
                }
                if (returnValue > 0)
                    return true;
                else
                    return false;
            }
        }

        /// <summary>
        /// Purpose: Validating user on user login.
        /// </summary>
        public static bool validateUser(User user, int role)
        {
            String strConnString = ConfigurationManager.ConnectionStrings["connectionString"].ConnectionString;

            using (SqlConnection connect = new SqlConnection(strConnString))
            {
                SqlCommand myCommand = new SqlCommand();
                int returnValue;
                try
                {
                    myCommand.CommandType = CommandType.StoredProcedure;
                    myCommand.CommandText = "ValidateUserLogin";
                    myCommand.Parameters.Add("@Email", SqlDbType.NVarChar).Value = user.email;
                    myCommand.Parameters.Add("@UsersPassword", SqlDbType.NVarChar).Value = user.password;
                    myCommand.Parameters.Add("@RoleId", SqlDbType.Int).Value = role;
                    myCommand.Connection = connect;
                    connect.Open();
                    int.TryParse(myCommand.ExecuteScalar().ToString(), out returnValue);
                    //myCommand.ExecuteScalar();
                    //returnValue = myCommand.ExecuteNonQuery();
                }
                finally
                {
                    connect.Close();
                    connect.Dispose();
                }
                if (returnValue > 0)
                    return true;
                else
                    return false;
            }
        }

        ///// <summary>
        ///// Purpose: Validating Supplier on Supplier login.
        ///// </summary>
        ///// 
        //public static bool validateSupplier(User user, int role)
        //{
        //    String strConnString = ConfigurationManager.ConnectionStrings["connectionString"].ConnectionString;

        //    using (SqlConnection connect = new SqlConnection(strConnString))
        //    {
        //        SqlCommand myCommand = new SqlCommand();
        //        int returnValue;
        //        myCommand.CommandType = CommandType.StoredProcedure;
        //        myCommand.CommandText = "ValidateUserLogin";
        //        try
        //        {
        //            myCommand.Parameters.Add("@Email", SqlDbType.NVarChar).Value = user.email;
        //            myCommand.Parameters.Add("@UsersPassword", SqlDbType.NVarChar).Value = user.password;
        //            myCommand.Parameters.Add("@RoleId", SqlDbType.Int).Value = role;
        //            myCommand.Connection = connect;
        //            connect.Open();
        //            int.TryParse(myCommand.ExecuteScalar().ToString(), out returnValue);
        //            user.usersId = returnValue; /*(int)myCommand.ExecuteScalar();*/
        //        }
        //        catch (Exception ex)
        //        {
        //            throw ex;
        //        }
        //        finally
        //        {
        //            connect.Close();
        //            connect.Dispose();
        //        }
        //        if (returnValue > 0)
        //            return true;
        //        else
        //            return false;
        //    }
        //}

        /// <summary>
        /// Purpose:Method fetching address detail of user and returning to set sessionValue.
        /// </summary>
        /// <param name="email"></param>
        /// <returns></returns>
        public Address getUserAddress(int userId)
        {
            SqlConnection connection = new SqlConnection(strConnString);
            string commandString = "SELECT Addresses, City, PostalCode FROM Address WHERE UsersId = @UsersId AND DeliveryContact IS NULL";
            using (SqlCommand myCommand = new SqlCommand(commandString, connection))
            {
                myCommand.Parameters.AddWithValue("@UsersId", userId);
                connection.Open();
                using (SqlDataReader sqlReader = myCommand.ExecuteReader())
                {
                    var address = new Address();
                    try
                    {
                        while (sqlReader.Read())
                        {
                            address.addresses = sqlReader["Addresses"].ToString();
                            address.city = sqlReader["City"].ToString();
                            address.postalCode = sqlReader["PostalCode"].ToString();
                        }
                    }
                    finally
                    {
                        myCommand.Dispose();
                        connection.Close();
                    }
                    return address;
                }
            }
        }

        /// <summary>
        /// Purpose:Method fetching user details and returning to set sessionValue.
        /// </summary>
        /// <param name="email"></param>
        /// <returns></returns>
        public User getUserDetail(string email)
        {
            SqlConnection connection = new SqlConnection(strConnString);
            string commandString = "SELECT FirstName, LastName, Mobile, Email, UsersId FROM UsersDetail where Email='" + email + "'";
            using (SqlCommand command = new SqlCommand(commandString, connection))
            {
                connection.Open();
                using (SqlDataReader sqlReader = command.ExecuteReader())
                {
                    var user = new User();
                    try
                    {
                        while (sqlReader.Read())
                        {
                            user.firstName = sqlReader["FirstName"].ToString();
                            user.lastName = sqlReader["LastName"].ToString();
                            user.mobileNumber = sqlReader["Mobile"].ToString();
                            user.email = sqlReader["Email"].ToString();
                            user.usersId = Convert.ToInt32(sqlReader["UsersId"]);
                        }
                    }
                    finally
                    {
                        command.Dispose();
                        connection.Close();
                    }
                    return user;
                }
            }
        }

        /// <summary>
        /// Purose:Method saving the order of the product by user details filled in order modal.
        /// </summary>
        /// <param name="address"></param>
        /// <param name="orderDetail"></param>
        /// <param name="order"></param>
        /// <returns></returns>
        public bool saveOrderDll(Address address, OrderDetail orderDetail, Order order)
        {
            using (SqlConnection connection = new SqlConnection(strConnString))
            {
                SqlCommand myCommand = new SqlCommand();
                int returnValue;
                myCommand.CommandType = CommandType.StoredProcedure;
                myCommand.CommandText = "SaveOrder";
                try
                {
                    myCommand.Parameters.Add("@Addresses", SqlDbType.NVarChar).Value = address.addresses;
                    myCommand.Parameters.Add("@City", SqlDbType.NVarChar).Value = address.city;
                    myCommand.Parameters.Add("@PostalCode", SqlDbType.NVarChar).Value = address.postalCode;
                    myCommand.Parameters.Add("@DeliveryContact", SqlDbType.NVarChar).Value = address.deliveryContact;
                    myCommand.Parameters.Add("@Name", SqlDbType.NVarChar).Value = address.name;
                    myCommand.Parameters.Add("@OrderDate", SqlDbType.DateTime).Value = order.orderDate;
                    myCommand.Parameters.Add("@OrderStatus", SqlDbType.NVarChar).Value = order.orderStatus;
                    myCommand.Parameters.Add("@UsersId", SqlDbType.Int).Value = order.usersId;
                    myCommand.Parameters.Add("@ProductId", SqlDbType.Int).Value = orderDetail.productId;
                    myCommand.Parameters.Add("@Quantity", SqlDbType.Int).Value = orderDetail.quantity;

                    myCommand.Connection = connection;
                    connection.Open();
                    int.TryParse(myCommand.ExecuteScalar().ToString(), out returnValue);
                }
                finally
                {
                    connection.Close();
                    connection.Dispose();
                }
                if (returnValue > 0)
                    return true;
                else
                    return false;
            }
        }

        /// <summary>
        /// Purpose: Fetching users Order.
        /// </summary>
        /// <param name="UserId"></param>
        /// <returns></returns>
        public DataSet getOrder(int userId)
        {
            using (SqlConnection connection = new SqlConnection(strConnString))
            {
                string commandString = "GetOrders";
                using (SqlCommand myCommand = new SqlCommand(commandString, connection))
                {
                    DataSet dataSet = new DataSet();
                    try
                    {
                        myCommand.CommandType = CommandType.StoredProcedure;
                        myCommand.Parameters.AddWithValue("@UsersId", userId);
                        SqlDataAdapter dataAdapter = new SqlDataAdapter(myCommand);
                        dataAdapter.Fill(dataSet);
                    }
                    finally
                    {
                        myCommand.Dispose();
                    }
                    return dataSet;
                }
            }
        }

        /// <summary>
        /// Purpose: Fetching Orders Detail.
        /// </summary>
        /// <param name="ordersId"></param>
        /// <returns></returns>
        public DataTable getOrdersDetail(int ordersId)
        {
            using (SqlConnection connection = new SqlConnection(strConnString))
            {
                string commandString = "GetOrderDetail";
                using (SqlCommand myCommand = new SqlCommand(commandString, connection))
                {
                    DataTable dataTable = new DataTable();
                    try
                    {
                        myCommand.CommandType = CommandType.StoredProcedure;
                        myCommand.Parameters.AddWithValue("@OrdersId", ordersId);
                        SqlDataAdapter dataAdapter = new SqlDataAdapter(myCommand);
                        dataAdapter.Fill(dataTable);
                    }
                    finally
                    {
                        myCommand.Dispose();
                    }
                    return dataTable;
                }
            }
        }

        /// <summary>
        /// Purpose: Inserting details for cart on cart table.
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="productId"></param>
        /// <returns></returns>
        public int addProductCart(int userId, int productId)
        {
            using (SqlConnection connection = new SqlConnection(strConnString))
            {
                string commandString = "InsertCartDetail";
                using (SqlCommand myCommand = new SqlCommand(commandString, connection))
                {
                    int returnValue;
                    myCommand.CommandType = CommandType.StoredProcedure;
                    try
                    {
                        myCommand.Parameters.Add("@UsersId", SqlDbType.Int).Value = userId;
                        myCommand.Parameters.Add("@ProductId", SqlDbType.Int).Value = productId;
                        myCommand.Parameters.Add("@ProductQuantity", SqlDbType.Int).Value = 1;
                        myCommand.Connection = connection;
                        connection.Open();
                        returnValue = myCommand.ExecuteNonQuery();
                    }
                    finally
                    {
                        connection.Close();
                        connection.Dispose();
                    }
                    return returnValue;
                }
            }
        }

        /// <summary>
        /// Purpose: Getting cart count from cart table and keeping in session.
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public int getCartCount(int userId)
        {
            using (SqlConnection connection = new SqlConnection(strConnString))
            {
                string commandString = "GetCartCount";
                using (SqlCommand myCommand = new SqlCommand(commandString, connection))
                {
                    int count;
                    try
                    {
                        myCommand.CommandType = CommandType.StoredProcedure;
                        myCommand.Parameters.Add("@UsersId", SqlDbType.Int).Value = userId;
                        myCommand.Connection = connection;
                        connection.Open();
                        int.TryParse(myCommand.ExecuteScalar().ToString(), out count);
                    }
                    finally
                    {
                        connection.Close();
                        connection.Dispose();
                    }
                    return count;
                }
            }
        }

        /// <summary>
        /// Purpose: Fetching cart Product details on click of cart in navbar.
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public DataTable getCartProduct(int userId)
        {
            using (SqlConnection connection = new SqlConnection(strConnString))
            {
                string commandString = "GetCartProductDetail";
                using (SqlCommand myCommand = new SqlCommand(commandString, connection))
                {
                    DataTable dataTable = new DataTable();
                    try
                    {
                        myCommand.CommandType = CommandType.StoredProcedure;
                        myCommand.Parameters.AddWithValue("@UsersId", userId);
                        SqlDataAdapter dataAdapter = new SqlDataAdapter(myCommand);
                        dataAdapter.Fill(dataTable);
                    }
                    finally
                    {
                        myCommand.Dispose();
                    }
                    return dataTable;
                }
            }
        }

        /// <summary>
        /// Purpose: Delete the product from cart on click of remove button.
        /// </summary>
        /// <param name="productId"></param>
        /// <returns></returns>
        public bool deleteCartProduct(int productId)
        {
            using (SqlConnection connection = new SqlConnection(strConnString))
            {
                string commandString = "DeleteCartProduct";
                using (SqlCommand myCommand = new SqlCommand(commandString, connection))
                {
                    myCommand.CommandType = CommandType.StoredProcedure;
                    int returnValue = 0;

                    myCommand.Parameters.Add("@ProductId", SqlDbType.Int).Value = productId;
                    myCommand.Connection = connection;
                    try
                    {
                        connection.Open();
                        returnValue = myCommand.ExecuteNonQuery();
                    }
                    finally
                    {
                        connection.Close();
                        connection.Dispose();
                    }
                    if (returnValue > 0)
                        return true;
                    else
                        return false;
                }

            }
        }

        /// <summary>
        /// Purpose: Fetching the productId of product in cart of user.
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public List<Product> getCartDetail(int userId)
        {
            SqlConnection connection = new SqlConnection(strConnString);
            string commandString = "SELECT ProductId FROM Cart WHERE UsersId = @UsersId";
            using (SqlCommand myCommand = new SqlCommand(commandString, connection))
            {
                myCommand.Parameters.AddWithValue("@UsersId", userId);
                connection.Open();
                using (SqlDataReader sqlReader = myCommand.ExecuteReader())
                {
                    List<Product> list = new List<Product>();
                    try
                    {
                        while (sqlReader.Read())
                        {
                            list.Add(new Product());
                            list[list.Count - 1].productId = Convert.ToInt32((sqlReader["ProductId"]).ToString());
                        }
                    }
                    finally
                    {
                        myCommand.Dispose();
                        connection.Close();
                    }
                    return list;
                }
            }
        }

        /// <summary>
        /// Purpose: Inserting order from cart.
        /// </summary>
        /// <param name="address"></param>
        /// <param name="order"></param>
        /// <param name="list"></param>
        /// <returns></returns>
        public bool insertCartOrder(Address address, Order order, string productIds, string inStock, string quantity)
        {
            using (SqlConnection connection = new SqlConnection(strConnString))
            {
                int returnValue;
                SqlCommand myCommand = new SqlCommand();
                myCommand.Connection = connection;
                try
                {
                    myCommand.CommandType = CommandType.StoredProcedure;
                    myCommand.CommandText = "InsertCartOrder";

                    myCommand.Parameters.Add("@Name", SqlDbType.NVarChar).Value = address.name;
                    myCommand.Parameters.Add("@Addresses", SqlDbType.NVarChar).Value = address.addresses;
                    myCommand.Parameters.Add("@City", SqlDbType.NVarChar).Value = address.city;
                    myCommand.Parameters.Add("@PostalCode", SqlDbType.NVarChar).Value = address.postalCode;
                    myCommand.Parameters.Add("@DeliveryContact", SqlDbType.NVarChar).Value = address.deliveryContact;
                    myCommand.Parameters.Add("@UsersId", SqlDbType.Int).Value = order.usersId;
                    myCommand.Parameters.Add("@OrderDate", SqlDbType.DateTime).Value = order.orderDate;
                    myCommand.Parameters.Add("@OrderStatus", SqlDbType.NVarChar).Value = order.orderStatus;
                    myCommand.Parameters.Add("@ProductIds", SqlDbType.NVarChar).Value = productIds;
                    myCommand.Parameters.Add("@InStocks", SqlDbType.NVarChar).Value = inStock;
                    myCommand.Parameters.Add("@Quantity", SqlDbType.NVarChar).Value = quantity;

                    connection.Open();
                    int.TryParse(myCommand.ExecuteScalar().ToString(), out returnValue);
                }
                finally
                {
                    connection.Close();
                    connection.Dispose();
                }
                if (returnValue > 0)
                    return true;
                else
                    return false;
            }
        }

        /// <summary>
        /// Purpose: Updating user detail from editUserProfile page.
        /// </summary>
        /// <param name="address"></param>
        /// <param name="user"></param>
        /// <returns></returns>
        public bool updateUser(Address address, User user)
        {
            int returnValue;
            using (SqlConnection connection = new SqlConnection(strConnString))
            {
                try
                {
                    SqlCommand myCommand = new SqlCommand();
                    myCommand.CommandType = CommandType.StoredProcedure;
                    myCommand.CommandText = "UpdateUserDetail";

                    myCommand.Parameters.Add("@FirstName", SqlDbType.NVarChar).Value = user.firstName;
                    myCommand.Parameters.Add("@LastName", SqlDbType.NVarChar).Value = user.lastName;
                    myCommand.Parameters.Add("@Mobile", SqlDbType.NVarChar).Value = user.mobileNumber;
                    myCommand.Parameters.Add("@Password", SqlDbType.NVarChar).Value = user.password;
                    myCommand.Parameters.Add("@UserId", SqlDbType.Int).Value = user.usersId;
                    myCommand.Parameters.Add("@Addresses", SqlDbType.NVarChar).Value = address.addresses;
                    myCommand.Parameters.Add("@City", SqlDbType.NVarChar).Value = address.city;
                    //myCommand.Parameters.Add("@State", SqlDbType.NVarChar).Value = order.orderStatus;
                    myCommand.Parameters.Add("@PostalCode", SqlDbType.NVarChar).Value = address.postalCode;
                    myCommand.Connection = connection;
                    connection.Open();
                    int.TryParse(myCommand.ExecuteScalar().ToString(), out returnValue);
                }
                finally
                {
                    connection.Close();
                    connection.Dispose();
                }
                if (returnValue > 0)
                    return true;
                else
                    return false;
            }
        }

        /// <summary>
        /// Purpose: Fetch password of user on request forgot password.
        /// </summary>
        /// <param name="email"></param>
        /// <returns></returns>
        public string fetchPassword(string email)
        {
            string password = "";
            using (SqlConnection connection = new SqlConnection(strConnString))
            {
                using (SqlCommand myCommand = new SqlCommand("SELECT [UsersPassword] FROM UsersDetail WHERE Email = @Email"))
                {
                    try
                    {
                        myCommand.Parameters.AddWithValue("@Email", email);
                        myCommand.Connection = connection;
                        connection.Open();
                        using (SqlDataReader dataReader = myCommand.ExecuteReader())
                        {
                            if (dataReader.Read())
                            {
                                password = dataReader["UsersPassword"].ToString();
                            }
                        }
                    }
                    finally
                    {
                        connection.Close();
                    }
                }
            }
            return password;
        }

        /// <summary>
        /// Purpose: Fetch product on basis of category for productList page
        /// </summary>
        /// <param name="categoryId"></param>
        /// <returns></returns>
        public DataTable getProductList(int categoryId)
        {
            using (SqlConnection connection = new SqlConnection(strConnString))
            {
                string commandstring = "GetProductList";
                using (SqlCommand myCommand = new SqlCommand(commandstring, connection))
                {
                    DataTable dataTable = new DataTable();
                    try
                    {
                        myCommand.CommandType = CommandType.StoredProcedure;
                        myCommand.Parameters.AddWithValue("@CategoryId", categoryId);
                        SqlDataAdapter dataAdapter = new SqlDataAdapter(myCommand);
                        dataAdapter.Fill(dataTable);
                    }
                    finally
                    {
                        connection.Close();
                    }
                    return dataTable;
                }
            }
        }

        /// <summary>
        /// Purpose: Fetching categories from ProductCategory table and displaying in categories page.
        /// </summary>
        /// <returns></returns>
        public List<ProductCategory> listCategory()
        {
            using (SqlConnection connection = new SqlConnection(strConnString))
            {
                string commandString = "SELECT ProductCategoryId, CategoryName, CategoryImage FROM ProductCategory";
                List<ProductCategory> category = new List<ProductCategory>();
                try
                {
                    using (SqlCommand myCommand = new SqlCommand(commandString, connection))
                    {
                        connection.Open();
                        using (SqlDataReader dataReader = myCommand.ExecuteReader())
                        {
                            while (dataReader.Read())
                            {
                                category.Add(new ProductCategory
                                {
                                    productCategoryId = Convert.ToInt32(dataReader["ProductCategoryId"]),
                                    categoryName = Convert.ToString(dataReader["CategoryName"]),
                                    categoryImage = Convert.ToString(dataReader["CategoryImage"])
                                });
                            }
                        }
                    }
                }
                finally
                {
                    connection.Close();
                }
                return category;
            }
        }

        /// <summary>
        /// Purpose: Fetching product by word from search area. 
        /// </summary>
        /// <param name="searchWord"></param>
        /// <returns></returns>
        public List<Product> searchItem(string searchWord)
        {
            using (SqlConnection connection = new SqlConnection(strConnString))
            {
                string commandstring = "SearchItem";
                using (SqlCommand myCommand = new SqlCommand(commandstring, connection))
                {
                    List<Product> items = new List<Product>();
                    try
                    {
                        myCommand.CommandType = CommandType.StoredProcedure;
                        myCommand.Parameters.AddWithValue("@SearchWord", searchWord);
                        connection.Open();
                        using (SqlDataReader dataReader = myCommand.ExecuteReader())
                        {
                            while (dataReader.Read())
                            {
                                items.Add(new Product
                                {
                                    productId = Convert.ToInt32(dataReader["ProductId"]),
                                    productName = Convert.ToString(dataReader["Name"]),
                                    unitPrice = Convert.ToInt32(dataReader["UnitPrice"]),
                                    inStock = Convert.ToInt32(dataReader["InStock"]),
                                    description = Convert.ToString(dataReader["Descriptions"]),
                                    picture = Convert.ToString(dataReader["Picture"]),
                                });
                            }
                        }
                    }
                    finally
                    {
                        connection.Close();
                    }
                    return items;
                }
            }
        }

        /// <summary>
        /// Purpose: updating product quantity in cart.
        /// </summary>
        /// <param name="ProductId"></param>
        /// <param name="quantity"></param>
        /// <param name="userId"></param>
        /// <returns></returns>
        public bool updateCartQuantity(int productId, int quantity, int userId)
        {
            int returnValue;
            using (SqlConnection connection = new SqlConnection(strConnString))
            {
                try
                {
                    SqlCommand myCommand = new SqlCommand();
                    myCommand.CommandType = CommandType.StoredProcedure;
                    myCommand.CommandText = "UpdateCartQuantity";

                    myCommand.Parameters.Add("@UserId", SqlDbType.Int).Value = userId;
                    myCommand.Parameters.Add("@ProductId", SqlDbType.Int).Value = productId;
                    myCommand.Parameters.Add("@Quantity", SqlDbType.Int).Value = quantity;
                    myCommand.Connection = connection;
                    connection.Open();
                    returnValue = myCommand.ExecuteNonQuery();
                }
                finally
                {
                    connection.Close();
                    connection.Dispose();
                }
                if (returnValue > 0)
                    return true;
                else
                    return false;
            }
        }
    }
}
