﻿$(document).ready(function () {
    var errorFlag = 0;
    //var letters = /^[A-Za-z]+$/; 
    var checkMail = /^([a-zA-Z0-9.])+@([a-zA-Z0-9-])+[.]([a-zA-Z0-9]{2,4})+$/;  //email allows letters, numbers and periods

    //Binding list of countries.
    $.ajax({
        type: "POST",
        url: "RegisterSupplier.aspx/countryList",
        data: "{}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (msg) {
            $("#listCountry").get(0).options.length = 0;
            $("#listCountry").get(0).options[0] = new Option("--Select Country--", "-1");

            $.each(msg.d, function (index, item) {
                $("#listCountry").get(0).options[$("#listCountry").get(0).options.length] = new Option(item.Value, item.Key);
            });
        },
        error: function () {
            //alert("Failed to load Country");
        }
    });

    //Onselect of country calls method to bind states.
    $("#listCountry").bind("change", function () {
        getState($(this).val());
    });

    //function to bind the list of states on selected country.
    function getState(countryId) {
        if (countryId > 0) {
            var data = '{"countryId":' + countryId + "}"
            var obj = { country: $.parseJSON(data) }
            $.ajax({
                type: "POST",
                url: "RegisterSupplier.aspx/stateList",
                data: JSON.stringify(obj),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (msg) {
                    $("#listState").get(0).options.length = 0;
                    $("#listState").get(0).options[0] = new Option("--Select State--", "-1");

                    $.each(msg.d, function (index, item) {
                        $("#listState").get(0).options[$("#listState").get(0).options.length] = new Option(item.Value, item.Key);
                    });
                },
                error: function () {
                    $("#listState").get(0).options.length = 0;
                    //alert("Failed to load states.");
                }
            });
        }
        else {
            $("#listState").get(0).options.length = 0;
        }
    }

    //For drop down on hover.
    $(function () {
        $(".dropdown").hover(
                function () {
                    $('.dropdown-menu', this).stop(true, true).fadeIn("fast");
                    $(this).toggleClass('open');
                    $('b', this).toggleClass("caret caret-up");
                },
                function () {
                    $('.dropdown-menu', this).stop(true, true).fadeOut("fast");
                    $(this).toggleClass('open');
                    $('b', this).toggleClass("caret caret-up");
                });
    });

    //On scroll of home page hide navbar and fix header section.
    $(window).scroll(function () {
        if ($(window).scrollTop() > 10) {
            $('#homeHeader').addClass('header-squize');
        }
        if ($(window).scrollTop() < 11) {
            $('#homeHeader').removeClass('header-squize');
        }
        if ($(window).scrollTop() > 25) {
            $('#homeHeader').addClass('header-fixed');
        }
        if ($(window).scrollTop() < 26) {
            $('#homeHeader').removeClass('header-fixed');
        }
    });

    //Display an error message on invalid input type.
    function errorDisplay(elementId, message) {
        $(elementId).parent().addClass("has-error has-feedback");
        $(elementId).next().html('<p class ="text-danger small"><small>' + message + '</small></p>');
    }

    //Removes the warning on key press.
    $("input").keypress(function () {
        $(this).parent().removeClass("has-error has-feedback");
        $(this).next().html('');
        errorFlag = 0;
    });

    //Removes error warning from state list.
    $("#listState").click(function () {
        $(this).parent().removeClass("has-error has-feedback");
        $(this).next().html('');
        errorFlag = 0;
    });

    //Removes error warning from country list.
    $("#listCountry").click(function () {
        $(this).parent().removeClass("has-error has-feedback");
        $(this).next().html('');
        errorFlag = 0;
    });

    //redirect to same page on click of registersupplier in navbar of RegisterSupplier page.
    $("#anchorRegisterSupplier").click(function () {
        window.location.href = 'RegisterSupplier.aspx';
    });

    //open suplier login modal on clicking login in navbar of RegisterSupplier page .
    $("#anchorSupplierLogin").click(function () {
        $("#txtLoginSupplier").parent().removeClass("has-error has-feedback");
        $("#txtLoginSupplier").next().html('');
        $("#pwdSupplierPassword").parent().removeClass("has-error has-feedback");
        $("#pwdSupplierPassword").next().html('');
        $("#txtLoginSupplier").val('');
        $("#pwdSupplierPassword").val('');
        $("#supplierLoginModal").modal("show");
    });

    //Redirect to home page on click of home on navbar of RegistrationSupplier page.
    $("#anchorSupplierHome").click(function () {
        window.location.href = 'HomePage.aspx';
    });

    //redirecting on RegisterSupplier Page on clicking Register link in navbar of HomePage.
    $("#anchorSupplier").click(function () {
        window.location.href = 'RegisterSupplier.aspx';
    });

    //Validating and registering supplier on clicking Register button of RegisterSupplier page.
    $("#butSubmit").click(function () {
        var firstName = $.trim($("#txtFirstName").val());
        var lastName = $.trim($("#txtLastName").val());
        var email = $.trim($("#txtEmailId").val());
        var password = $.trim($("#pwdPassword").val());
        var address = $.trim($("#txtAddress").val());
        var city = $.trim($("#txtCity").val());
        var country = $.trim($("#listCountry").val());
        var state = $.trim($("#listState").val());
        var zipCode = $.trim($("#txtZipCode").val());
        var mobileNumber = $.trim($("#txtMobile").val());

        if (firstName == "") {
            errorDisplay("#txtFirstName", "Enter first name");
            errorFlag = 1;
        }

        if (lastName == "") {
            errorDisplay("#txtLastName", "Enter last name");
            errorFlag = 1;
        }

        if (password == "") {
            errorDisplay("#pwdPassword", "Enter password");
            errorFlag = 1;
        }

        if (address == "") {
            errorDisplay("#txtAddress", "Enter address");
            errorFlag = 1;
        }

        if (email == "" || !email.match(checkMail)) {
            errorDisplay("#txtEmailId", "Enter valid email address (letters,numbers and periods)");
            errorFlag = 1;
        }

        if (city == "") {
            errorDisplay("#txtCity", "Enter city");
            errorFlag = 1;
        }

        if (zipCode == "") {
            errorDisplay("#txtZipCode", "Enter zip code");
            errorFlag = 1;
        }

        if (mobileNumber == "") {
            errorDisplay("#txtMobile", "Enter mobile number");
            errorFlag = 1;
        }

        if (state == "" || state == "undefined" || state == "--Select State--") {
            errorDisplay("#listState", "Select State");
            errorFlag = 1;
        }

        if (country == -1 || country == "" || country == "undefined") {
            errorDisplay("#listCountry", "Select Country");
            errorFlag = 1;
        }

        if (errorFlag == 0) {
            var dataUser = "{";
            $(".User input").each(function () {
                dataUser += '"' + $(this).attr("name") + '":"' + $(this).val() + '",';
            });
            dataUser = dataUser.slice(0, dataUser.length - 1) + " }";
            var objUser = $.parseJSON(dataUser)

            var dataAddress = "{";
            $(".Address input").each(function () {
                dataAddress += '"' + $(this).attr("name") + '":"' + $(this).val() + '",';
            });
            dataAddress += '"stateId":' + $("#listState").val() + ',';

            dataAddress = dataAddress.slice(0, dataAddress.length - 1) + " }";
            var objAddress = $.parseJSON(dataAddress)

            $.ajax({
                type: "POST",
                url: "RegisterSupplier.aspx/supplierSignup",
                data: JSON.stringify({ user: objUser, address: objAddress }), //JSON.stringify(objUser, objAddress),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (msg) {
                    if (msg.d == 0) {
                        alert("User already exists.")
                    }
                    else if (msg.d == 1) {
                        alert("Registered sucessfully.");
                        window.location.href = 'RegisterSupplier.aspx';
                    }
                    else {
                        alert("Oops!!!Something went wrong.");
                    }
                },
                error: function (msg) {
                    alert("Error occured on connection with ajax call");
                }
            });

        }

    });

});