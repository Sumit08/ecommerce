﻿$(document).ready(function () {
    var errorFlag = 0;
    //var letters = /^[A-Za-z]+$/; 
    var checkMail = /^([a-zA-Z0-9.])+@([a-zA-Z0-9-])+[.]([a-zA-Z0-9]{2,4})+$/;  //email allows letters, numbers and periods
    //alert("supplierloginmodal");

    //Display an error message on invalid input type.
    function errorDisplay(elementId, message) {
        $(elementId).parent().addClass("has-error has-feedback");
        $(elementId).next().html('<span class="glyphicon glyphicon-remove form-control-feedback"></span><p class ="text-danger small"><small>' + message + '</small></p>');
    }

    //Removes the warning on key press.
    $("input").keypress(function () {
        $(this).parent().removeClass("has-error has-feedback");
        $(this).next().html('');
        errorFlag = 0;
    });

    //hiding supplier login modal on clicking register link of supplier login modal.
    $("#anchorModalRegister").click(function () {
        $("#supplierLoginModal").modal("hide");
    });

    //click on logout by supplier redirect to HomePage.
    $("#ddlSupplierLogout").click(function () {
        $.ajax({
            type: "POST",
            url: "RegisterSupplier.aspx/killSupplierSession",
            data: {},
            contentType: "application/json; charset=utf-8",
            dataType: "json"
        }).done(function () {
            window.location.href = 'HomePage.aspx';
        });
    });

    //Logging in the validSuplier on clicking Login button of Supplier login modal.
    $("#btnLoginModalSupplier").click(function () {
        var email = $.trim($("#txtLoginSupplier").val());
        var password = $.trim($("#pwdSupplierPassword").val());

        if (email == "" || !email.match(checkMail)) {
            errorDisplay("#txtLoginSupplier", "Enter valid email address (letters,numbers and periods)");
            errorFlag = 1;
        }

        if (password == "") {
            errorDisplay("#pwdSupplierPassword", "Enter password");
            errorFlag = 1;
        }

        if (errorFlag == 0) {
            var data = "{";
            $(".Login input").each(function () {
                data += '"' + $(this).attr("name") + '":"' + btoa($(this).val()) + '",';
            });
            data = data.slice(0, data.length - 1) + " }";
            var obj = { user: $.parseJSON(data) }

            $.ajax({
                type: "POST",
                url: "RegisterSupplier.aspx/supplierLogin",
                data: JSON.stringify(obj),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (msg) {
                    if (msg.d == 0) {
                        alert("Invalid User");
                        $("#txtLoginSupplier").val('');
                        $("#pwdSupplierPassword").val('');
                    }
                    else if (msg.d == 1) {
                        window.location.href = 'RegisterSupplier.aspx';
                    }
                    else {
                        alert("Oops!!!\nSomething went wrong.");
                    }
                },
                error: function (msg) {
                    alert("Error occured on connection.");
                }
            });
        }
    });

    //Rediecting to AddProductpage on click Supplier ddl AddProduct.
    $("#ddlSupplierAddProduct").click(function () {
        window.location.href = 'AddProduct.aspx';
    });
});