﻿/// <reference path="jquery.min.js" />

$(document).ready(function () {
    var ddlValue = $.trim($("#ddlProduct").val());
    var errorFlag = 0;

    //View image before upload.
    $("#fileImage").on('change', function (ev) {
        var file = ev.target.files[0];
        var fileReader = new FileReader();

        fileReader.onload = function (ev2) {
            //console.dir(ev2);
            $('#imgPrev').attr('src', ev2.target.result);
        };

        fileReader.readAsDataURL(file);
    });


    //Display an error message on invalid input type.
    function errorDisplay(elementId, message) {
        $(elementId).parent().addClass("has-error has-feedback");
        $(elementId).next().html('<p class ="text-danger small"><small>' + message + '</small></p>');
    }

    //Removes the warning on key press.
    $(".Product input").keypress(function () {
        $(this).parent().removeClass("has-error has-feedback");
        $(this).next().html('');
        errorFlag = 0;
    });
    $(".Product textarea").keypress(function () {
        $(this).parent().removeClass("has-error has-feedback");
        $(this).next().html('');
        errorFlag = 0;
    });
    //$("#fileImage").click(function () {
    //    $(this).parent().removeClass("has-error has-feedback");
    //    $(this).next().html('');
    //    errorFlag = 0;
    //});

    //Enables and disabels AddNew and Update button and productName textbox.
    {
        if (ddlValue == "0") {
            $("#butAddNewProduct").prop('disabled', false);
            $("#butUpdateProduct").prop('disabled', true);
        }
        else {
            $("#butAddNewProduct").prop('disabled', true);
            $("#butUpdateProduct").prop('disabled', false);
            $("#txtProductName").attr('readonly', true);
        }
    }

    //Validating AddProduct Page.
    function validate() {
        var productName = $.trim($("#txtProductName").val());
        var unitPrice = $.trim($("#numUnitPrice").val());
        var unitWeight = $.trim($("#numUnitWeight").val());
        var inStock = $.trim($("#numInStock").val());
        var discount = $.trim($("#numDiscount").val());
        //var picture = $.trim($("#fileImage").val());
        var description = $.trim($("#txtAreaDescription").val());
        var isChecked = $("#chkGridView").is(":checked");

        if (productName == "") {
            errorDisplay("#txtProductName", "Enter product name");
            errorFlag = 1;
        }

        if (unitPrice == "" || unitPrice == 0) {
            errorDisplay("#numUnitPrice", "Enter price");
            errorFlag = 1;
        }

        if (unitWeight == "" || unitWeight == 0) {
            errorDisplay("#numUnitWeight", "Enter weight");
            errorFlag = 1;
        }

        if (inStock == "") {
            errorDisplay("#numInStock", "Enter stock");
            errorFlag = 1;
        }

        if (discount == "") {
            errorDisplay("#numDiscount", "Enter discount percentage");
            errorFlag = 1;
        }
        if (description == "") {
            errorDisplay("#txtAreaDescription", "Enter description");
            errorFlag = 1;
        }
        //if (picture == "") {
        //    errorDisplay("#fileImage", "Upload picture");
        //    errorFlag = 1;
        //}
        if (isChecked) {
            alert("Please check the category.")
            errorFlag = 1;
        }
    }

    //calling validate method and adding new product on clicking AddNew button of AddProduct page.
    $("#butAddNewProduct").click(function () {
        validate();
        if (errorFlag == 0) {

            var fileUpload = $("#fileImage").get(0);
            var files = fileUpload.files;
            var imgData = new FormData();
            for (var i = 0; i < files.length; i++) {
                imgData.append(files[i].name, files[i]);
            }
            $.ajax({
                url: "UploadHandler.ashx",
                type: "POST",
                contentType: false,
                processData: false,
                data: imgData,
                succss: function (result) {
                    alert(result);
                },
                error: function (err) {
                    alert(err.statusText);
                }
            });

            var data = "{";
            $(".Product input").each(function () {
                data += '"' + $(this).attr("name") + '":"' + $(this).val().replace(/\\/g, "\\\\") + '",';
                //or
                //obj[$(this.attr("name"))] = $(this).val();
            });


            $(".Product textarea").each(function () {
                data += '"' + $(this).attr("name") + '":"' + $.trim($(this).val()) + '",';
            });

            {
                data += '"productCategories":[';
                $(".Product input:checkbox:checked").each(function () {
                    data += '{"productCategoryId":' + $(this).parents("tr").children(".categoryId").html() + "},";
                });
            }
            data = data.slice(0, data.length - 1) + " ]}";

            // data = data.slice(0, data.length - 1) + " }";
            var obj = { product: $.parseJSON(data) }

            $.ajax({
                type: "POST",
                url: "AddProduct.aspx/newProduct",
                data: JSON.stringify(obj),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (msg) {
                    if (msg.d == 0) {
                        alert("You already have product with same name.");
                    }
                    else if (msg.d == 1) {
                        alert("Product added successfully.");
                        window.location.href = 'AddProduct.aspx';
                    }
                    else {
                        alert("Oops!!!\nSomething went wrong.");
                    }
                },
                error: function (msg) {
                    alert("Error occured on connection.");
                }
            });
        }
    });

    //calling validate method and updating the existing product on clicking Update button of AddProduct page.
    $("#butUpdateProduct").click(function () {
        validate();
        if (errorFlag == 0) {
            var fileUpload = $("#fileImage").get(0);
            var files = fileUpload.files;
            var imgData = new FormData();
            for (var i = 0; i < files.length; i++) {
                imgData.append(files[i].name, files[i]);
            }
            $.ajax({
                url: "UploadHandler.ashx",
                type: "POST",
                contentType: false,
                processData: false,
                data: imgData,
                succss: function (result) {
                    alert(result);
                },
                error: function (err) {
                    alert(err.statusText);
                }
            });

            var data = "{";

            $(".Product input").each(function () {
                data += '"' + $(this).attr("name") + '":"' + $(this).val().replace(/\\/g, "\\\\") + '",';
            });

            $(".Product textarea").each(function () {
                data += '"' + $(this).attr("name") + '":"' + $(this).val().trim() + '",';
            });

            data += '"productId":' + $("#ddlProduct :selected ").val() + ',';

            {
                data += '"productCategories":[';
                $(".Product input:checkbox:checked").each(function () {
                    data += '{"productCategoryId":' + $(this).parents("tr").children(".categoryId").html() + "},";
                });
            }
            data = data.slice(0, data.length - 1) + " ]}";
            var obj = { product: $.parseJSON(data) }

            $.ajax({
                type: "POST",
                url: "AddProduct.aspx/update",
                data: JSON.stringify(obj),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (msg) {
                    if (msg.d == 0) {
                        alert("Cannot be updated.");
                    }
                    else if(msg.d==1){
                        alert("Product updated successfully.");
                        window.location.href = 'AddProduct.aspx';
                    }
                    else {
                        alert("Oops!!!\nSomething went wrong.")
                    }
                },
                error: function (msg) {
                    alert("Error occured on connection.");
                }
            });
        }
    });

});