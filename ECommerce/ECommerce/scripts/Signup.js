﻿$(document).ready(function () {
    var errorFlag = 0;
    //var letters = /^[A-Za-z]+$/; 
    var checkMail = /^([a-zA-Z0-9.])+@([a-zA-Z0-9-])+[.]([a-zA-Z0-9]{2,4})+$/;  //email allows letters, numbers and periods

    //On scroll of home page hide navbar and fix header section.
    $(window).scroll(function () {
        if ($(window).scrollTop() > 10)
        {
            $('#homeHeader').addClass('header-squize');
        }
        if ($(window).scrollTop() < 11) {
            $('#homeHeader').removeClass('header-squize');
        }
        if ($(window).scrollTop() > 20) {
            $('#homeHeader').addClass('header-fixed');
        }
        if ($(window).scrollTop() < 21) {
            $('#homeHeader').removeClass('header-fixed');
        }
    });

    //For drop down on hover.
    $(function () {
        $(".dropdown").hover(
                function () {
                    $('.dropdown-menu', this).stop(true, true).fadeIn("fast");
                    $(this).toggleClass('open');
                    $('b', this).toggleClass("caret caret-up");
                },
                function () {
                    $('.dropdown-menu', this).stop(true, true).fadeOut("fast");
                    $(this).toggleClass('open');
                    $('b', this).toggleClass("caret caret-up");
                });
    });

    //Display an error message on invalid input type.
    function errorDisplay(elementId, message) {
        $(elementId).parent().addClass("has-error has-feedback");
        $(elementId).next().html('<span class="glyphicon glyphicon-remove form-control-feedback"></span><p class ="text-danger small"><small>' + message + '</small></p>');
    }

    //Removes the warning on key press.
    $("input").keypress(function () {
        $(this).parent().removeClass("has-error has-feedback");
        $(this).next().html('');
        errorFlag = 0;
    });

    //opening signup modal on clicking signup link in HomePage navbar.
    $("#anchorSignup").click(function () {
        $("#txtSignupUser").parent().removeClass("has-error has-feedback");
        $("#txtSignupUser").next().html('');
        $("#pwdSignupPassword").parent().removeClass("has-error has-feedback");
        $("#pwdSignupPassword").next().html('');
        $("#txtSignupUser").val('');
        $("#pwdSignupPassword").val('');
        $("#signupModal").modal("show");
    });

    //switching to user signup modal on clicking signup link, in login modal.
    $("#toSignupModal").click(function () {
        $("#txtSignupUser").parent().removeClass("has-error has-feedback");
        $("#txtSignupUser").next().html('');
        $("#pwdSignupPassword").parent().removeClass("has-error has-feedback");
        $("#pwdSignupPassword").next().html('');
        $("#loginModal").modal("hide");
        $("#txtSignupUser").val('');
        $("#pwdSignupPassword").val('');
        $("#signupModal").modal("show");
    });

    //Signing the new User on signup button click of signup modal.
    $("#btnModalSignup").click(function () {
        var email = $.trim($("#txtSignupUser").val());
        var password = $.trim($("#pwdSignupPassword").val());

        if (email == "" || !email.match(checkMail)) {
            errorDisplay("#txtSignupUser", "Enter valid email address (letters,numbers and periods)");
            errorFlag = 1;
        }

        if (password == "") {
            errorDisplay("#pwdSignupPassword", "Enter password");
            errorFlag = 1;
        }

        if (errorFlag == 0) {
            var data = "{";
            $(".Signup input").each(function () {
                data += '"' + $(this).attr("name") + '":"' + btoa($(this).val()) + '",';
            });
            data = data.slice(0, data.length - 1) + " }";
            var obj = { user: $.parseJSON(data) }

            $.ajax({
                type: "POST",
                url: "HomePage.aspx/userSignup",
                data: JSON.stringify(obj),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (msg) {
                    if (msg.d == 0) {
                        alert("User already exists.")
                        $("#txtSignupUser").val('');
                        $("#pwdSignupPassword").val('');
                    }
                    else if (msg.d == 1) {
                        alert("Account created sucessfully.");
                        $("#signupModal").modal("hide");
                        $("#loginModal").modal("show");
                    }
                    else {
                        alert("Oops!!!\nSomething went wrong.")
                    }
                },
                error: function (msg) {
                    alert("Error occcured on connection.")
                }
            });
        }

    });
});

