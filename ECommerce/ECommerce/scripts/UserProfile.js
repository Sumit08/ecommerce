﻿/// <reference path="jquery.min.js" />

$(document).ready(function () {
    var errorFlag = 0;
    $("#txtProfileEmailId").attr('readonly', true);

    //Binding list of countries.
    $.ajax({
        type: "POST",
        url: "EditUserProfile.aspx/countryList",
        data: "{}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (msg) {
            $("#profileListCountry").get(0).options.length = 0;
            $("#profileListCountry").get(0).options[0] = new Option("--Select Country--", "-1");

            $.each(msg.d, function (index, item) {
                $("#profileListCountry").get(0).options[$("#profileListCountry").get(0).options.length] = new Option(item.Value, item.Key);

                $("#profileListCountry :select").val('3');
            });
        },
        error: function () {
            //alert("Failed to load Country");
        }
    });

    //Onselect of country calls method to bind states.
    $("#profileListCountry").bind("change", function () {
        getState($(this).val());
    });

    //function to bind the list of states on selected country.
    function getState(countryId) {
        if (countryId > 0) {
            var data = '{"countryId":' + countryId + "}"
            var obj = { country: $.parseJSON(data) }
            $.ajax({
                type: "POST",
                url: "EditUserProfile.aspx/stateList",
                data: JSON.stringify(obj),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (msg) {
                    $("#profileListState").get(0).options.length = 0;
                    $("#profileListState").get(0).options[0] = new Option("--Select State--", "-1");

                    $.each(msg.d, function (index, item) {
                        $("#profileListState").get(0).options[$("#profileListState").get(0).options.length] = new Option(item.Value, item.Key);
                    });
                },
                error: function () {
                    $("#profileListState").get(0).options.length = 0;
                }
            });
        }
        else {
            $("#profileListState").get(0).options.length = 0;
        }
    }

    //Validating order modal.
    function validate() {
        var password = $.trim($("#pwdProfilePassword").val());

        if (password == "") {
            errorDisplay("#pwdProfilePassword", "Enter password");
            errorFlag = 1;
        }
    }

    //Display an error message on invalid input type.
    function errorDisplay(elementId, message) {
        $(elementId).parent().addClass("has-error has-feedback");
        $(elementId).next().html('<p class ="text-danger small"><small>' + message + '</small></p>');
    }

    //Removes the warning on key press.
    $(".User input").keypress(function () {
        $(this).parent().removeClass("has-error has-feedback");
        $(this).next().html('');
        errorFlag = 0;
    });

    //click on user profile logout, killing session and redirecting to home page.
    $("#userProfileLogout").click(function () {
        $.ajax({
            type: "POST",
            url: "HomePage.aspx/killUserSession",
            data: {},
            contentType: "application/json; charset=utf-8",
            dataType: "json"
        }).done(function () {
            window.location.href = 'HomePage.aspx';
        });
    });

    //click on user EditProfile save button. updates userDetail. 
    $("#btnSaveUserProfile").click(function () {
        validate();
        if (errorFlag == 0) {

            var dataAddress = "{";
            $(".Address input").each(function () {
                dataAddress += '"' + $(this).attr("name") + '":"' + $(this).val() + '",';
            });
            dataAddress = dataAddress.slice(0, dataAddress.length - 1) + " }";

            var dataUser = "{";
            $(".User input").each(function () {
                dataUser += '"' + $(this).attr("name") + '":"' + $(this).val() + '",';
            });
            dataUser = dataUser.slice(0, dataUser.length - 1) + " }";

            var obj = { address: $.parseJSON(dataAddress), user: $.parseJSON(dataUser) }

            $.ajax({
                type: "POST",
                url: "EditUserProfile.aspx/profile",
                data: JSON.stringify(obj),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (msg) {
                    if (msg.d == 0) {
                        alert("Cannot update your profile.");

                    }
                    else if (msg.d == 1) {
                        alert("Profile updated successfully.");
                        window.location.href = 'UserProfile.aspx';
                    }
                    else {
                        alert("Oops!!!\nSomething went wrong.");
                    }
                },
                error: function (msg) {
                    alert("Error occured on connection.");
                }
            });
        }
    });
});