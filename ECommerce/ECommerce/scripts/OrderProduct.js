﻿$(document).ready(function () {
    var errorFlag = 0;
    //var letters = /^[A-Za-z]+$/; 
    var checkMail = /^([a-zA-Z0-9.])+@([a-zA-Z0-9-])+[.]([a-zA-Z0-9]{2,4})+$/;  //email allows letters, numbers and periods
    var url = window.location.href;

    //validating order modal.
    function validate() {
        var Quantity = $.trim($("#numQuantity").val());
        var firstName = $.trim($("#txtShipFirstName").val());
        var lastName = $.trim($("#txtShipLastName").val());
        var address = $.trim($("#txtShipAddress").val());
        var city = $.trim($("#txtShipCity").val());
        var postalCode = $.trim($("#txtShipPostalCode").val());
        var mobileNumber = $.trim($("#txtShipMobileNumber").val());

        if (Quantity <= 0) {
            errorDisplay("#numQuantity", "positive value greater than 0.");
            errorFlag = 1;
        }

        if (firstName == "") {
            errorDisplay("#txtShipFirstName", "Enter first name");
            errorFlag = 1;
        }

        if (lastName == "") {
            errorDisplay("#txtShipLastName", "Enter last name");
            errorFlag = 1;
        }

        if (address == "") {
            errorDisplay("#txtShipAddress", "Enter address");
            errorFlag = 1;
        }

        if (city == "") {
            errorDisplay("#txtShipCity", "Enter city name");
            errorFlag = 1;
        }

        if (postalCode == "") {
            errorDisplay("#txtShipPostalCode", "Enter postal code");
            errorFlag = 1;
        }

        if (mobileNumber == "") {
            errorDisplay("#txtShipMobileNumber", "Enter mobile number");
            errorFlag = 1;
        }
    }

    //display an error message on invalid input type.
    function errorDisplay(elementId, message) {
        $(elementId).parent().addClass("has-error has-feedback");
        //$(elementId).next().html('<span class="glyphicon glyphicon-remove form-control-feedback"></span><p class ="text-danger small"><small>' + message + '</small></p>');
    }

    //removes the warning on key press.
    $("input").keypress(function () {
        $(this).parent().removeClass("has-error has-feedback");
        //$(this).next().html('');
        errorFlag = 0;
    });

    //click on buyNow button of ProductDetail page.
    $("#btnBuyNow").click(function () {
        $.ajax({
            type: "POST",
            url: "ProductDetail.aspx/checkSession",
            data: {},
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (msg) {
                if (msg.d == false) {
                    $("#loginModal").modal("show");
                }
                else {
                    var instock = parseInt(($("#paraInStock").text().split(' ')).shift());
                    var quantity = parseInt($("#numQuantity").val());
                    var unitPrice = parseInt(($("#paraUnitPrice").text().split(' ')).shift());
                    $("#paraTotalAmount").text((quantity * unitPrice).toString() + " Rs");

                    $("#numQuantity").on("keypress", false);
                    $("input").parent().removeClass("has-error has-feedback");
                    $("#txtShipFirstName").val('');
                    $("#txtShipLastName").val('');
                    $("#txtShipAddress").val('');
                    $("#txtShipCity").val('');
                    $("#txtShipPostalCode").val('');
                    $("#txtShipMobileNumber").val('');
                    $("#numQuantity").prop('max', instock);
                    $('#chkAddress').attr('checked', false);
                    $('#txtFirstName').prop("disabled", true);
                    $('#txtLastName').prop("disabled", true);
                    $('#txtAddress').prop("disabled", true);
                    $('#txtCity').prop("disabled", true);
                    $('#txtPostalCode').prop("disabled", true);
                    $('#txtMobileNumber').prop("disabled", true);
                    $("#placeOrderModal").modal("show");
                }
            },
            error: function (msg) {
                alert("Error occured on connection with ajax call");
            }
        });
    });

    //check box to copy the address to shipping address.
    $("#chkAddress").click(function () {
        $("#txtShipFirstName").val($("#txtFirstName").val());
        $("#txtShipLastName").val($("#txtLastName").val());
        $("#txtShipAddress").val($("#txtAddress").val());
        $("#txtShipCity").val($("#txtCity").val());
        $("#txtShipPostalCode").val($("#txtPostalCode").val());
        $("#txtShipMobileNumber").val($("#txtMobileNumber").val());

        if ($("#chkAddress").prop("checked") == false) {
            $("#txtShipFirstName").val('');
            $("#txtShipLastName").val('');
            $("#txtShipAddress").val('');
            $("#txtShipCity").val('');
            $("#txtShipPostalCode").val('');
            $("#txtShipMobileNumber").val('');
        }
        else {
            $("input").parent().removeClass("has-error has-feedback");
        }
    });

    //calculating totalAmount.
    $("#numQuantity").on('blur', function () {
        var quantity = parseInt($("#numQuantity").val());
        var unitPrice = parseInt(($("#paraUnitPrice").text().split(' ')).shift());
        $("#paraTotalAmount").text((quantity * unitPrice).toString() + " Rs");
    });

    //onclick of submit button of order modal.
    $("#btnOrderModalSubmit").click(function () {
        validate();
        if (errorFlag == 0) {
            var productId = parseInt((window.location.href.split("?")[1]).split("=")[1]);

            var dataAddress = "{";
            $(".Address input").each(function () {
                dataAddress += '"' + $(this).attr("name") + '":"' + $(this).val() + '",';
            });
            dataAddress = dataAddress.slice(0, dataAddress.length - 1) + " }";

            var dataOrderDetail = "{";
            $(".OrderDetail input").each(function () {
                dataOrderDetail += '"' + $(this).attr("name") + '":' + $(this).val() + ',';
            });
            dataOrderDetail += '"productId":' + productId + ',';
            dataOrderDetail = dataOrderDetail.slice(0, dataOrderDetail.length - 1) + " }";

            //var dataProduct = '{"productId":' + productId + "}";

            var obj = { address: $.parseJSON(dataAddress), orderDetail: $.parseJSON(dataOrderDetail) }

            $.ajax({
                type: "POST",
                url: "ProductDetail.aspx/saveOrder",
                data: JSON.stringify(obj),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (msg) {
                    if (msg.d == 0) {
                        alert("Order cannot be placed.Please Try again.");
                    }
                    else if (msg.d == 1) {
                        window.location.href = 'AfterOrder.aspx';
                    }
                    else {
                        alert("Oops!!!\nSomething went wrong.");
                    }
                },
                error: function (msg) {
                    alert("Error occured on connection.");
                }
            });
        }
    });

    //print oreder detail.
    $("#printIcon").click(function () {
        var printsection = document.getElementById("OrderDetailArea").innerHTML;
        var printWindow = window.open('', '', 'height=800,width=1200');
        printWindow.document.write('<html><head><title>TrendsEden</title>');
        printWindow.document.write('</head><body >');
        printWindow.document.write(printsection);
        printWindow.document.write('</body></html>');
        printWindow.document.close();
        printWindow.print();
    })

    //disable BuyNoW and AddTo Cart button if stock is zero.
    var stock = $("#chkStockValue").text();
    if (stock == "In Stock: 0") {
        $("#btnBuyNow").prop('disabled', true);
        $("#btnAddCart").prop('disabled', true);
    }

    //redirecting to home page on clicking to continue shopping.
    $("#btnContinueShopping").click(function () {
        window.location.href = 'HomePage.aspx';
    });

});

