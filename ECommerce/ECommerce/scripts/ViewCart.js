﻿$(document).ready(function () {
    var url = window.location.href;
    var errorFlag = 0;

    //Validating order modal.
    function validate() {
        var Quantity = $.trim($("#numQuantity").val());
        var firstName = $.trim($("#txtShipFirstName").val());
        var lastName = $.trim($("#txtShipLastName").val());
        var address = $.trim($("#txtShipAddress").val());
        var city = $.trim($("#txtShipCity").val());
        var postalCode = $.trim($("#txtShipPostalCode").val());
        var mobileNumber = $.trim($("#txtShipMobileNumber").val());

        if (firstName == "") {
            errorDisplay("#txtShipFirstName", "Enter first name");
            errorFlag = 1;
        }

        if (lastName == "") {
            errorDisplay("#txtShipLastName", "Enter last name");
            errorFlag = 1;
        }

        if (address == "") {
            errorDisplay("#txtShipAddress", "Enter address");
            errorFlag = 1;
        }

        if (city == "") {
            errorDisplay("#txtShipCity", "Enter city name");
            errorFlag = 1;
        }

        if (postalCode == "") {
            errorDisplay("#txtShipPostalCode", "Enter postal code");
            errorFlag = 1;
        }

        if (mobileNumber == "") {
            errorDisplay("#txtShipMobileNumber", "Enter mobile number");
            errorFlag = 1;
        }
    }

    //alerting in case error on filling the field.
    function errorDisplay(elementId, message) {
        $(elementId).parent().addClass("has-error has-feedback");
    }

    //Removes the warning on key press.
    $("input").keypress(function () {
        $(this).parent().removeClass("has-error has-feedback");
        errorFlag = 0;
    });

    //Disabeling keys for inputs the quantity of product.
    //$("#numCartQuantity").on("keypress", false);

    //Disabeling the BUY button if their is no product in cart to buy.
    if ($("#paraName").text() == "") {
        $("#btnCartBuy").prop("disabled", true);
    }

    //click on AddToProduct button of ProductDetail page.
    $("#btnAddCart").click(function () {
        var productId = parseInt((window.location.href.split("?")[1]).split("=")[1]);
        var dataProduct = '{"productId":' + productId + "}";
        var obj = { product: $.parseJSON(dataProduct) }
        $.ajax({
            type: "POST",
            url: "ProductDetail.aspx/checkSessionCart",
            data: JSON.stringify(obj),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (msg) {
                if (msg.d == false) {
                    $("#loginModal").modal("show");
                }
                else {
                    $.ajax({
                        type: "POST",
                        url: "ProductDetail.aspx/countAddCart",
                        data: JSON.stringify(obj),
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        success: function (msg) {
                            if (msg.d == 0) {
                                alert("Item already in cart");
                            }
                            else if (msg.d == 1) {
                                alert("Item added to cart");
                                window.location.href = url;
                            }
                            else {
                                alert("Oops!!!\nSomething went wrong.");
                            }
                        },
                        error: function (msg) {
                            alert("Error occured on connection.");
                        }
                    });
                }
            },
            error: function (msg) {
                alert("Error occured on connection.");
            }
        });
    });

    //onClick of BUY button of cart modal.
    $("#btnCartBuy").click(function () {
        $("input").parent().removeClass("has-error has-feedback");
        $("#txtShipFirstName").val('');
        $("#txtShipLastName").val('');
        $("#txtShipAddress").val('');
        $("#txtShipCity").val('');
        $("#txtShipPostalCode").val('');
        $("#txtShipMobileNumber").val('');
        $('#chkCartAddress').attr('checked', false);
        $('#txtFirstName').prop("disabled", true);
        $('#txtLastName').prop("disabled", true);
        $('#txtAddress').prop("disabled", true);
        $('#txtCity').prop("disabled", true);
        $('#txtPostalCode').prop("disabled", true);
        $('#txtMobileNumber').prop("disabled", true);
        $("#cartModal").modal("show");
    });

    //check box to copy the address to shipping address.
    $("#chkCartAddress").click(function () {
        $("#txtShipFirstName").val($("#txtFirstName").val());
        $("#txtShipLastName").val($("#txtLastName").val());
        $("#txtShipAddress").val($("#txtAddress").val());
        $("#txtShipCity").val($("#txtCity").val());
        $("#txtShipPostalCode").val($("#txtPostalCode").val());
        $("#txtShipMobileNumber").val($("#txtMobileNumber").val());

        if ($("#chkCartAddress").prop("checked") == false) {
            $("#txtShipFirstName").val('');
            $("#txtShipLastName").val('');
            $("#txtShipAddress").val('');
            $("#txtShipCity").val('');
            $("#txtShipPostalCode").val('');
            $("#txtShipMobileNumber").val('');
        }
        else {
            $("input").parent().removeClass("has-error has-feedback");
        }


    });

    //placing order from cart on click buy from cart.
    $("#btnCartModalSubmit").click(function () {
        validate();
        if (errorFlag == 0) {
            var dataAddress = "{";
            $(".Address input").each(function () {
                dataAddress += '"' + $(this).attr("name") + '":"' + $(this).val() + '",';
            });
            dataAddress = dataAddress.slice(0, dataAddress.length - 1) + " }";

            var quantity = 0;
            $('.qty').each(function () {
                alert("hi");
            });

            var obj = { address: $.parseJSON(dataAddress) }

            $.ajax({
                type: "POST",
                url: "ViewCart.aspx/saveCartOrder",
                data: JSON.stringify(obj),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (msg) {
                    if (msg.d == 0) {
                        alert("Order cannot be placed.Please Try again.");
                    }
                    else if (msg.d == 1) {
                        window.location.href = 'AfterOrder.aspx';
                    }
                    else {
                        alert("Oops!!!\nSomething went wrong.");
                    }
                },
                error: function (msg) {
                    alert("Error occured on connection.");
                }
            });
        }
    });

    //calculating total amount for product in cart as per quantity.
    $(".txtQuantity").blur(function () {
        var Qty = parseFloat($(this).val());
        var stock = parseInt($(this).closest('.row').find('#paraCartInstock').html().split("InStock:")[1]);
        var unitPrice = parseFloat($(this).closest('.row').find('#price').html().split("Rs.")[1]);
        if (isNaN(Qty) || Qty <= 0 || Qty > stock) {
            $(this).val(1);
            Qty = 1;
        }
        var unitTotal = (Qty * unitPrice);
        $(this).closest('.row').find('#unitTotal').html("Rs. " + unitTotal.toFixed(2));
        var payableAmount = 0;

        $('.para').each(function () {
            payableAmount += parseFloat($(this).closest('.row').find('#unitTotal').html().split("Rs. ")[1]);
        })
        $("#payableAmount").html("Amount Payable: Rs. " + payableAmount.toFixed(2));

        //Updating cart quantity.
        var productId = parseInt($(this).closest('.row').find('#hiddenProductId').val());
        $.ajax({
            type: "POST",
            url: "ViewCart.aspx/updateCart",
            data: '{"productId":' + productId + ', "quantity":' + Qty + "}",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (msg) {
                },
            error: function (msg) {
                alert("Oops!!\nSomething went wrong.");
            }
        });

    });
});



