﻿$(document).ready(function () {
    var errorFlag = 0;
    //var letters = /^[A-Za-z]+$/; 
    var checkMail = /^([a-zA-Z0-9.])+@([a-zA-Z0-9-])+[.]([a-zA-Z0-9]{2,4})+$/;  //email allows letters, numbers and periods

    //Display an error message on invalid input type.
    function errorDisplay(elementId, message) {
        $(elementId).parent().addClass("has-error has-feedback");
        $(elementId).next().html('<span class="glyphicon glyphicon-remove form-control-feedback"></span><p class ="text-danger small"><small>' + message + '</small></p>');
    }

    //Removes the warning on key press.
    $("input").keypress(function () {
        $(this).parent().removeClass("has-error has-feedback");
        $(this).next().html('');
        errorFlag = 0;
    });

    //opening login modal on clicking login link in HomePage navbar.
    $("#anchorLogin").click(function () {
        $("#txtLoginUser").parent().removeClass("has-error has-feedback");
        $("#txtLoginUser").next().html('');
        $("#pwdLoginPassword").parent().removeClass("has-error has-feedback");
        $("#pwdLoginPassword").next().html('');
        $("#txtLoginUser").val('');
        $("#pwdLoginPassword").val('');
        $("#loginModal").modal("show");
    });

    //switching to user login modal on clicking login link, in signup modal.
    $("#toLoginModal").click(function () {
        $("#signupModal").modal("hide");
        $("#txtLoginUser").parent().removeClass("has-error has-feedback");
        $("#txtLoginUser").next().html('');
        $("#pwdLoginPassword").parent().removeClass("has-error has-feedback");
        $("#pwdLoginPassword").next().html('');
        $("#txtLoginUser").val('');
        $("#pwdLoginPassword").val('');
        $("#loginModal").modal("show");
    });

    //redirecting on home page on click users ddlLogout and killing session.
    $("#ddlUserLogout").click(function () {
        $.ajax({
            type: "POST",
            url: "HomePage.aspx/killUserSession",
            data: {},
            contentType: "application/json; charset=utf-8",
            dataType: "json"
            //success: function (msg) {
            //    alert("session killed.");

            //}
        }).done(function () {
            window.location.href = 'HomePage.aspx';
        });
    });

    //After validation, logging in the valid user on login button click of login modal.
    $("#btnModalLogin").click(function () {
        var url = window.location.href;
        var email = $.trim($("#txtLoginUser").val());
        var password = $.trim($("#pwdLoginPassword").val());

        if (email == "" || !email.match(checkMail)) {
            errorDisplay("#txtLoginUser", "Enter valid email address (letters,numbers and periods)");
            errorFlag = 1;
        }

        if (password == "") {
            errorDisplay("#pwdLoginPassword", "Enter password");
            errorFlag = 1;
        }

        if (errorFlag == 0) {
            var data = "{";
            $(".Login input").each(function () {
                data += '"' + $(this).attr("name") + '":"' + btoa($(this).val()) + '",';
                //data = data.replace("undefined", "null");
            });
            data = data.slice(0, data.length - 1) + " }";
            var obj = { user: $.parseJSON(data) }

            $.ajax({
                type: "POST",
                url: "HomePage.aspx/userLogin",
                data: JSON.stringify(obj),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (msg) {
                    if (msg.d == 0) {
                        alert("Invalid User.");
                        $("#txtLoginUser").val('');
                        $("#pwdLoginPassword").val('');
                    }
                    else if (msg.d == 1) {
                        window.location.href = url;
                    }
                    else {
                        alert("Oops!!!\nSomethong went wrong.");
                    }
                },
                error: function (msg) {
                    alert("Error occured on connection.");
                }
            });
        }
    });

    //Forgot password link.
    $("#linkForgotPassword").click(function () {
        $("#loginModal").modal("hide");
        $("#txtEmail").val('');
        $("#passwordRecoveryModal").modal("show");
    });

    //sending mail on click of ok button of PasswordRecovery modal.
    $("#btnPwdRecovery").click(function () {
        var email = $.trim($("#txtEmailPasswordRecovery").val());
        if (email == "" || !email.match(checkMail)) {
            errorDisplay("#txtEmailPasswordRecovery", "Enter valid email address (letters,numbers and periods)");
            errorFlag = 1;
        }
        if (errorFlag == 0) {
            var data = '{"email":"' + email + '"}'

            var obj = { user: $.parseJSON(data) }

            $.ajax({
                type: "POST",
                url: "HomePage.aspx/passwordRecovery",
                data: JSON.stringify(obj),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (msg) {
                    if (msg.d == 0) {
                        alert("Entered email is not registered with us.");
                    }
                    else if (msg.d == 1) {
                        window.location.href = 'HomePage.aspx';
                        alert("Password is sent to your email.");
                    }
                    else {
                        alert("Oops !!!\n Something went wrong.");
                    }
                },
                error: function (msg) {
                    alert("Error occured on connection.");
                }
            });
        }
    })

    //Search Button click.
    //$("#btnSearch").click(function () {
    //    var search = $("#txtSearchItem").val();
    //    if(search != "")
    //    {
    //        alert("searching");
    //        $.ajax({
    //            type: "POST",
    //            url: "searchedProduct.aspx/searchItem",
    //            data: JSON.stringify({ searchWord: search}),
    //            contentType: "application/json; charset=utf-8",
    //            dataType: "json",
    //            success: function (msg) {
    //                if (msg.d == 0) {
    //                    alert("sucess");
    //                }
    //                else if (msg.d == 1) {
    //                    alert("failure");
    //                }
    //                else {
    //                    alert("Oops !!!\n Something went wrong.");
    //                }
    //            },
    //            error: function (msg) {
    //                alert("Error occured on connection.");
    //            }
    //        });
    //    }
    //});
});

