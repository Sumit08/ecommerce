﻿<%@ Page Title="" Language="C#" MasterPageFile="~/UserDetail.Master" AutoEventWireup="true" CodeBehind="ProductList.aspx.cs" Inherits="ECommerce.ProductList" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="col-md-2">
    </div>

    <div class="col-md-8" style="margin-top: 30px;">
        <div class="profile-content" style="margin-top: 30px; margin-bottom:30px; min-height: 350px">
            <asp:ListView ID="listProduct" runat="server" ClientIDMode="Static" OnPagePropertiesChanging="listProduct_PagePropertiesChanging" GroupPlaceholderID="groupPlaceHolder" ItemPlaceholderID="itemPlaceHolder">

                <LayoutTemplate>
                    <asp:PlaceHolder runat="server" ID="groupPlaceHolder"></asp:PlaceHolder>
                    <asp:DataPager ID="DataPager1" runat="server" PagedControlID="listProduct" PageSize="3">
                        <Fields>
                            <asp:NextPreviousPagerField ButtonType="Link" ShowFirstPageButton="false" ShowPreviousPageButton="true"
                                ShowNextPageButton="false" />
                            <asp:NumericPagerField ButtonType="Link" />
                            <asp:NextPreviousPagerField ButtonType="Link" ShowNextPageButton="true" ShowLastPageButton="false" ShowPreviousPageButton="false" />
                        </Fields>
                    </asp:DataPager>
                </LayoutTemplate>

                <GroupTemplate>
                    <tr>
                        <asp:PlaceHolder runat="server" ID="itemPlaceHolder"></asp:PlaceHolder>
                    </tr>
                </GroupTemplate>

                <ItemTemplate>
                    <div class="row">
                        <div class="col-md-3 ">
                            <a href="ProductDetail.aspx?ProductId=<%# Eval("ProductId")%>">
                                <img src="<%#:"images/uplaodProductImages/"+Eval("picture") %>" alt="image" height="160" width="150" />
                            </a>
                        </div>
                        <div class="col-md-6">
                            <label class="col-md-12"><%# Eval("Name")%></label>
                            <p class="col-md-12"><%# Eval("Descriptions")%></p>
                            <p class="col-md-12">Rs. <%# string.Format("{0:0.00}", Eval("UnitPrice"))%></p>
                        </div>
                        <div class="col-md-2">
                            <a href="ProductDetail.aspx?ProductId=<%# Eval("ProductId")%>">
                                <input type="button" class="col-md-12 btn btn-warning" id="btnBuyPL" value="VIEW" />
                            </a>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <br />
                        <br />
                    </div>
                </ItemTemplate>
            </asp:ListView>
        </div>
    </div>

</asp:Content>
