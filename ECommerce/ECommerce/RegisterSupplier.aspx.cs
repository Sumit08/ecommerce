﻿/************************************************************************************************
*Created By: Sumit
*Created On: 14 feb 2016
*Modified By:
*Modified On:
*Purpose: Register the supplier, validate on login, create sesssion for user and kill on logout.
*************************************************************************************************/

using BussinessLogic;
using ECommerce.Classes;
using EntityModel;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI.WebControls;

namespace ECommerce
{
    public partial class RegisterSupplier : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        /// <summary>
        /// Purpose: calling business logic insertSupplier of Register class. 
        /// </summary>
        [WebMethod]
        public static int supplierSignup(User user, Address address)
        {
            SuplierBll registerSupplier = new SuplierBll();
            try
            {
                registerSupplier.user = user;
                registerSupplier.address = address;
                return registerSupplier.InsertSupplier(registerSupplier);
            }
            catch (Exception ex)
            {
                Errors error = new Errors();
                error.LogError(ex);
                return (int)UserMessage.userMessage.exception;
            }

        }

        /// <summary>
        /// Purpose: Create Supplier session on sucessful login.
        /// </summary>
        [WebMethod]
        public static int supplierLogin(User user)
        {
            try
            {
                if (SuplierBll.authenticateSupplier(user))
                {
                    SessionValue sessionValue = new SessionValue();
                    var userDetail = sessionValue.getUserDetail(user.email);
                    HttpContext.Current.Session.Add("User", userDetail);
                    return (int)UserMessage.userMessage.success;
                }
                else
                {
                    return (int)UserMessage.userMessage.failure;
                }
            }
            catch (Exception ex)
            {
                Errors error = new Errors();
                error.LogError(ex);
                return (int)UserMessage.userMessage.exception;
            }
        }

        /// <summary>
        /// Purpose: Kill supplier session on supplier logout.
        /// </summary>
        [WebMethod]
        public static bool killSupplierSession()
        {
            HttpContext.Current.Session.Abandon();
            return true;
        }

        /// <summary>
        /// Purpose: Method for binding country.
        /// </summary>
        /// <returns></returns>
        [WebMethod]
        public static List<KeyValuePair<int, string>> countryList()
        {
            SuplierBll supplierBll = new SuplierBll();
            return supplierBll.getCountry().ToList();
        }

        /// <summary>
        /// Purpose: Method for binding state.
        /// </summary>
        /// <param name="country"></param>
        /// <returns></returns>
        [WebMethod]
        public static List<KeyValuePair<int, string>> stateList(Country country)
        {
            SuplierBll supplierBll = new SuplierBll();
            return supplierBll.getState(country.countryId).ToList();
        }
    }
}