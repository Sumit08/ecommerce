﻿/********************************************************************
*Created By: Sumit
*Created On: 16 Mar 2016
*Modified By:
*Modified On:
*Purpose: Call methods of UserBll class to get the details of order.
*********************************************************************/

using BussinessLogic;
using ECommerce.Classes;
using System;

namespace ECommerce
{
    public partial class OrderDetailPage : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (SessionHelper.User == null)
            {
                Response.Redirect("HomePage.aspx", true);
            }
            getOrderDetail();
        }

        /// <summary>
        /// Purpose: Calls getOrdersDetail method of UserBll class to get details of order.
        /// </summary>
        public void getOrderDetail()
        {
            int ordersId;
            UserBll userBll = new UserBll();
            try
            {
                int.TryParse(Request.QueryString["OrdersId"], out ordersId);
                listViewOrdersDetail.DataSource = userBll.getOrdersDetail(ordersId);
                listViewOrdersDetail.DataBind();
            }
            catch (Exception ex)
            {
                Errors error = new Errors();
                error.LogError(ex);
            }
        }
    }
}