﻿/***********************************************************************
*Created By: Sumit
*Created On: 14 mar 2016
*Modified By:
*Modified On:
*Purpose: Calls the method of BussinessLogic layer and handels error.
************************************************************************/

using BussinessLogic;
using EntityModel;
using System;
using System.Collections.Generic;
using System.Web;
using System.Web.Services;
using ECommerce.Classes;
using System.Configuration;

namespace ECommerce
{
    public partial class AddProduct : System.Web.UI.Page
    {
        protected string productName { get; set; }
        protected float unitPrice { get; set; }
        protected float unitWeight { get; set; }
        protected int inStock { get; set; }
        protected float discount { get; set; }
        protected string description { get; set; }
        protected string picture { get; set; }

        /// <summary>
        /// Purpose: calls the different binding methods and redirect the page on session status. 
        /// </summary>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (SessionHelper.User == null)
            {
                Response.Redirect("RegisterSupplier.aspx", true);
            }
            if (!IsPostBack)
            {
                bindGridView();
                bindProduct();
            }
        }

        /// <summary>
        /// Purpose: Method binding the gridView for ProductCategory.
        /// </summary>
        public void bindGridView()
        {
            SuplierBll suplierBll = new SuplierBll();
            try
            {
                List<ProductCategory> Categories = suplierBll.getCategory();
                gridView.DataSource = Categories;
                gridView.DataBind();
            }
            catch (Exception ex)
            {
                Errors error = new Errors();
                error.LogError(ex);
            }

        }

        /// <summary>
        /// Purpose: Method binding the products of that belongs to the supplier logged in.
        /// </summary>
        public void bindProduct()
        {
            int UserId = SessionHelper.User.userId;
            SuplierBll suplierBll = new SuplierBll();
            try
            {
                List<Product> productList = suplierBll.getProdut(UserId);
                ddlProduct.DataSource = productList;
                ddlProduct.DataTextField = "productName";
                ddlProduct.DataValueField = "productId";
                ddlProduct.DataBind();
            }
            catch (Exception ex)
            {
                Errors error = new Errors();
                error.LogError(ex);
            }
        }

        /// <summary>
        /// Purpose: Calling bussinesslogic addProduct method.
        /// </summary>
        [WebMethod]
        public static int newProduct(Product product)
        {
            SuplierBll addNewProduct = new SuplierBll();
            
            try
            {
                product.usersId = SessionHelper.User.userId;
                int ProductId = addNewProduct.addProduct(product);
                if (ProductId > 0)
                {
                    return (int)UserMessage.userMessage.success;
                }
                else
                {
                    return (int)UserMessage.userMessage.failure;
                }
            }
            catch (Exception ex)
            {
                Errors error = new Errors();
                error.LogError(ex);
                return (int)UserMessage.userMessage.exception;
            }

        }

        /// <summary>
        /// Purpose: Method on change of ddl is invoked and calls the bindProductField method of BussinessLogic.
        /// </summary>
        protected void ddlProductSelectedIndexChanged(object sender, EventArgs e)
        {

            SuplierBll suplierBll = new SuplierBll();
            try
            {
                int ProductId = Convert.ToInt32(ddlProduct.SelectedValue);
                var product = suplierBll.bindProductFields(ProductId);
                productName = product.productName;
                unitPrice = product.unitPrice;
                unitWeight = product.unitWeight;
                inStock = product.inStock;
                discount = product.discount;
                picture = (ConfigurationManager.AppSettings["imgPath"].ToString() + product.picture);
                description = product.description;
            }
            catch (Exception ex)
            {
                Errors error = new Errors();
                error.LogError(ex);
            }
        }

        /// <summary>
        /// Purpose: Calling bussinesslogic updateProduct method.
        /// </summary>
        [WebMethod]
        public static int update(Product product)
        {
            SuplierBll suplierBll = new SuplierBll();
            AddProduct addProduct = new AddProduct();
            try
            {
                if (suplierBll.updateProduct(product))
                {
                    return (int)UserMessage.userMessage.success;
                }
                else
                {
                    return (int)UserMessage.userMessage.failure;
                }
            }
            catch (Exception ex)
            {
                Errors error = new Errors();
                error.LogError(ex);
                return (int)UserMessage.userMessage.exception;
            }
        }
    }

}