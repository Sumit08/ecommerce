﻿/************************************************************************************
*Created By: Sumit
*Created On: 15 Mar 2016
*Modified By:
*Modified On:
*Purpose: Check the session value and according to it allow to acces UserProfile page.
**************************************************************************************/

using ECommerce.Classes;
using System;

namespace ECommerce
{
    public partial class UserProfile : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (SessionHelper.User == null)
            {
                Response.Redirect("HomePage.aspx", true);
            }
        }
    }
}