﻿<%@ Page Title="" Language="C#" MasterPageFile="~/UserDetail.Master" AutoEventWireup="true" CodeBehind="OrderDetailPage.aspx.cs" Inherits="ECommerce.OrderDetailPage" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <div class="col-md-2">
    </div>

    <div class="col-md-8">
        <div class="profile-content" style="min-height: 300px">
            <div class="row" id="OrderDetailArea">
                <div class="col-md-11">
                    <h3>My Orders Detail
                    </h3>
                </div>
                <div class="col-md-1">
                    <a href="javaScript:void(0);" id='printIcon'><span class="col-md-12 glyphicon glyphicon-print" style="font-size: 1.8em; color: green"></span>
                    </a>
                </div>
                <div class="col-md-12">
                    <hr />
                    <% var id = Request.QueryString["OrdersId"]; %>
                    <label class="col-md-3 ">OD20160<%=id %></label>
                    <label class="col-md-2 ">Name</label>
                    <label class="col-md-2">Unit Price</label>
                    <label class="col-md-2">Ordered Date</label>
                    <label class="col-md-2">Order Status</label>
                </div>
                <div class="col-md-12">
                    <hr />
                </div>
                <asp:ListView ID="listViewOrdersDetail" runat="server">
                    <ItemTemplate>
                        <div class="row">
                            <div class="col-md-2 col-md-offset-1">
                                <img src="<%# "images/uplaodProductImages/"+Eval("picture") %>" alt="image" height="150" width="100" />
                            </div>
                            <div class="col-md-2">
                                <label class="col-md-12"><%#  Eval("Name") %></label>
                                <div class="col-md-6">
                                    <p class="col-md-12">Qty:<%#  Eval("Quantity") %></p>
                                </div>
                            </div>
                            <div class="col-md-2">
                                <p class="col-md-12">Rs. <%# string.Format("{0:0.00}",Eval("UnitPrice")) %></p>
                            </div>
                            <div class="col-md-2">
                                <p class="col-md-12"><%#  Eval("OrderDate") %></p>
                            </div>
                            <div class="col-md-2">
                                <p class="col-md-12"><%#  Eval("OrderStatus") %></p>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <hr />
                        </div>
                    </ItemTemplate>
                </asp:ListView>
            </div>
        </div>
    </div>

</asp:Content>
