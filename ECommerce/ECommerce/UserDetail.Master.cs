﻿/****************************************************
*Created By: Sumit
*Created On: 13 feb 2016
*Modified By:
*Modified On:
*Purpose: Functionality required to dessign master page related for user.
*****************************************************/

using BussinessLogic;
using ECommerce.Classes;
using System;

namespace ECommerce
{
    public partial class Site : System.Web.UI.MasterPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            footerYear.InnerText = DateTime.Now.Year.ToString();

            cartSession();
        }

        /// <summary>
        /// Purpose: Creating session for cart count.
        /// </summary>
        public void cartSession()
        {
            if (SessionHelper.User != null)
            {
                UserBll userBll = new UserBll();
                int count = userBll.getCartCount(SessionHelper.User.userId);
                Session["Cart"] = count;
            }
        }

        /// <summary>
        /// Purpose: Pass the search word using query string to searchedProduct Page.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnSearch_ServerClick1(object sender, EventArgs e)
        {
            string searchWord = txtSearchItem.Value;
            if (searchWord != "" && searchWord != "%")
            {
                Response.Redirect("searchedProduct.aspx?search=" + searchWord, true);
            }
        }
    }
}