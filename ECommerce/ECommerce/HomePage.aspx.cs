﻿/*******************************************************************************************
*Created By: Sumit
*Created On: 13 feb 2016
*Modified By: 
*Modified On:
*Purpose: Creating and destroying session of user and calling the methods of Business logic.  
*********************************************************************************************/

using BussinessLogic;
using EntityModel;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Mail;
using System.Web;
using System.Web.Services;

namespace ECommerce
{
    public partial class Login : System.Web.UI.Page
    {

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                bindImageLandingPage();
            }
        }

        /// <summary>
        /// Purpose: Calling decodeUser method of BussinessLogic layer and returning bool type value to calling method. 
        /// </summary>
        [WebMethod]
        public static int userSignup(User user)
        {
            UserBll userBll = new UserBll();
            try
            {
                if (userBll.decodeUser(user))
                {
                    return (int)UserMessage.userMessage.success;
                }
                else
                {
                    return (int)UserMessage.userMessage.failure;
                }
            }
            catch (Exception ex)
            {
                Errors error = new Errors();
                error.LogError(ex);
                return (int)UserMessage.userMessage.exception;
            }

        }

        /// <summary>
        /// Purpose: Create user session on successfull login, calling authenticate method of BussinessLogic layer and returning bool value.
        /// </summary>
        [WebMethod]
        public static int userLogin(User user)
        {
            try
            {
                if (BussinessLogic.UserBll.authenticate(user))
                {
                    SessionValue sessionValue = new SessionValue();
                    var userDetail = sessionValue.getUserDetail(user.email);
                    HttpContext.Current.Session.Add("User", userDetail);
                    return (int)UserMessage.userMessage.success;
                }
                else
                {
                    return (int)UserMessage.userMessage.failure;
                }
            }
            catch (Exception ex)
            {
                Errors error = new Errors();
                error.LogError(ex);
                return (int)UserMessage.userMessage.exception;
            }

        }

        /// <summary>
        /// Purpose: Kill user session on user logout.
        /// </summary>
        [WebMethod]
        public static bool killUserSession()
        {
            HttpContext.Current.Session.Abandon();
            return true;
        }

        /// <summary>
        /// Purpose: Method on page load of landing page calls bindProductFields method of SuplierBll class of BussinessLogic.
        /// </summary>
        protected void bindImageLandingPage()
        {
            SuplierBll suplierBll = new SuplierBll();
            try
            {
                List<ProductCategory> categories = suplierBll.getProductByCategory();
                productCategoryList.DataSource = categories;
                productCategoryList.DataBind();
            }
            catch (Exception ex)
            {
                Errors error = new Errors();
                error.LogError(ex);
            }
        }

        /// <summary>
        /// Purpose: Recover password on request forgot password.
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        [WebMethod]
        public static int passwordRecovery(User user)
        {
            string email = user.email;
            UserBll userBll = new UserBll();
            var password = userBll.getPassword(email);
            string senderEmail = System.Web.Configuration.WebConfigurationManager.AppSettings["email"];
            string senderPassword = System.Web.Configuration.WebConfigurationManager.AppSettings["password"];
            if (password != "")
            {
                try
                {
                    var client = new SmtpClient("smtp.gmail.com", 587)
                    {
                        Credentials = new NetworkCredential(senderEmail, senderPassword),
                        EnableSsl = true
                    };
                    client.Send(senderEmail, "sumit.choudhary@mindfiresolutions.com", "Password Recovery", "Hi,\nYour password is " + password + "\n" + "Thank You");
                    return (int)UserMessage.userMessage.success;
                }
                catch (Exception ex)
                {
                    Errors error = new Errors();
                    error.LogError(ex);
                    return (int)UserMessage.userMessage.exception; ;
                }
            }
            else
            {
                return (int)UserMessage.userMessage.failure;
            }

        }
    }
}