﻿<%@ Page Title="" Language="C#" MasterPageFile="~/UserDetail.Master" AutoEventWireup="true" CodeBehind="searchedProduct.aspx.cs" Inherits="ECommerce.searchedProduct" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="loader">
    </div>
    <div class="col-md-2">
    </div>
    <div class="col-md-8">
        <div class="profile-content" style="margin-top: 30px; min-height: 300px">

            <asp:ListView ID="listSearchedProduct" runat="server">
                <ItemTemplate>
                    <div class="row" style="margin-top: 30px">
                        <div class="col-md-3 ">
                            <a href="ProductDetail.aspx?ProductId=<%# Eval("productId")%>" class="img-thumbnail">
                                <center><img src="<%#:"images/uplaodProductImages/"+Eval("picture") %>" alt="image" height="150" width="180" /></center>
                            </a>
                        </div>
                        <div class="col-md-6">
                            <label class="col-md-12"><%# Eval("productName")%></label>
                            <p class="col-md-12"><%# Eval("description")%></p>
                            <p class="col-md-12">Rs. <%# string.Format("{0:0.00}", Eval("unitPrice"))%></p>
                        </div>
                        <div class="col-md-2">
                            <a href="ProductDetail.aspx?ProductId=<%# Eval("productId")%>">
                                <input type="button" class="col-md-12 btn btn-warning" id="btnViewSearchedProduct" value="VIEW" />
                            </a>
                        </div>
                    </div>
                    <div class="col-md-12" style="margin-top: 50px">
                        <br />
                        <br />
                    </div>
                </ItemTemplate>
            </asp:ListView>
        </div>
    </div>
</asp:Content>
