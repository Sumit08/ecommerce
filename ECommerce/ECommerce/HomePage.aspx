﻿<%@ Page Title="" Language="C#" MasterPageFile="~/UserDetail.Master" AutoEventWireup="true" CodeBehind="HomePage.aspx.cs" Inherits="ECommerce.Login" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="css/User.css" rel="stylesheet" />
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <div class="container">
        <div class="row">
        </div>
        <br />
        <%--carousel--%>
        <div id="myCarousel" class="carousel slide" data-ride="carousel" style="width: 100%; height: 400px !important">
            <!-- Indicators -->
            <ol class="carousel-indicators">
                <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
                <li data-target="#myCarousel" data-slide-to="1"></li>
                <li data-target="#myCarousel" data-slide-to="2"></li>
                <li data-target="#myCarousel" data-slide-to="3"></li>
            </ol>

            <!-- Wrapper for slides -->
            <div class="carousel-inner" role="listbox">
                <div class="item active">
                    <img src="images/cloth_womens.jpg" alt="clothes" style="width: 850px; height: 400px" />
                </div>
                <div class="item">
                    <img src="images/Jeans.jpg" alt="Jeans" style="width: 850px; height: 400px" />
                </div>
                <div class="item">
                    <img src="images/mens_loafer_shoe.jpg" alt="Shoes" style="width: 850px; height: 400px" />
                </div>
                 <div class="item">
                    <img src="images/mes_cloth.jpg" alt="clothes" style="width: 850px; height: 400px" />
                </div>
                 <div class="item">
                    <img src="images/mobile.jpg" alt="mobile" style="width: 850px; height: 400px" />
                </div>
            </div>

            <!-- Left and right controls -->
            <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
                <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                <span class="sr-only">Previous</span>
            </a>
            <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
                <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                <span class="sr-only">Next</span>
            </a>
        </div>
        <br />
        <br />

        <%--dynamically creating pannel and binding product to it.--%>
        <asp:ListView ID="productCategoryList" runat="server" DataKeyNames="productCategoryId" ItemType="EntityModel.ProductCategory">
            <ItemTemplate>
                <div class="panel panel-default">
                    <div class="panel-heading panelHeader">
                        <%#: Item.categoryName %>
                    </div>
                    <div class="panel-body">
                        <asp:ListView ID="productList" ItemType="EntityModel.Product" DataSource="<%# Item.productList.Take(4) %>" runat="server" GroupItemCount="4">
                            <GroupTemplate>
                                <tr id="itemPlaceholderContainer" runat="server">
                                    <td id="itemPlaceholder" runat="server"></td>
                                </tr>
                            </GroupTemplate>
                            <ItemTemplate>
                                <!--BEGIN OF ITEM 1 -->
                                <div class="col-md-3 ">
                                    <div class="panel panel-default">
                                        <div class="panel-heading panelItemName">
                                            <%#: Item.productName%>
                                        </div>
                                        <div class="panel-body grow">
                                            <a href="ProductDetail.aspx?productID=<%#: Item.productId %>">
                                                <img src='<%#:"images/uplaodProductImages/"+Eval("picture") %>'
                                                    alt="image" class="img-responsive center-block" />
                                            </a>
                                        </div>
                                        <label style="text-align: center; display: block">
                                            Rs <%#: Eval("unitPrice") %>
                                        </label>
                                    </div>
                                </div>
                            </ItemTemplate>
                        </asp:ListView>
                    </div>
                </div>
            </ItemTemplate>
        </asp:ListView>
    </div>

</asp:Content>
