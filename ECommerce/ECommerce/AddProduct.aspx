﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Supplier.Master" AutoEventWireup="true" CodeBehind="AddProduct.aspx.cs" Inherits="ECommerce.AddProduct" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="col-md-3">
        <br />
        <asp:DropDownList class="col-md-6 col-md-offset-3" ID="ddlProduct" name="productId" runat="server" ClientIDMode="Static" AutoPostBack="true" OnSelectedIndexChanged="ddlProductSelectedIndexChanged">
        </asp:DropDownList>
    </div>

    <div class="col-md-5">
        <div class="form-group  col-md-6  Product">
            <label class="col-md-12 " for="txtProductName">Product Name<sup>*</sup></label>
            <input type="text" id="txtProductName" value="<%=productName %>" name="productName" class="form-control col-md-12" placeholder="Product name" />
            <div></div>
        </div>
        <div class="form-group col-md-6  Product">
            <label class="col-md-12 " for="numUnitPrice">Unit Price<sup>*</sup></label>
            <input type="number" id="numUnitPrice" value="<%=unitPrice %>" name="unitPrice" class="form-control col-md-12" step="any" />
            <div></div>
        </div>
        <div class="form-group col-md-6 Product">
            <label class="col-md-12 " for="numUnitWeight">Unit Weight<sup>*</sup></label>
            <input type="number" id="numUnitWeight" value="<%=unitWeight %>" name="unitWeight" class="form-control col-md-12 " step="any" />
            <div></div>
        </div>
        <div class="form-group col-md-6 Product">
            <label class="col-md-12 " for="numInStock">Instock<sup>*</sup></label>
            <input type="number" id="numInStock" value="<%=inStock %>" name="inStock" class="form-control col-md-12 " step="1" />
            <div></div>
        </div>
        <div class="form-group col-md-6 Product">
            <label class="col-md-12 " for="numDiscount">Discount<sup>*</sup></label>
            <input type="number" id="numDiscount" value="<%=discount %>" name="discount" class="form-control col-md-12" step="any" />
            <div></div>
        </div>
        <div class="form-group col-md-6 Product">
            <label class="col-md-12 " for="fileImage">Upload Picture<sup>*</sup></label>
            <input type="file" name="picture" id="fileImage" class="form-control col-md-12" "/>
            <div></div>
        </div>
        <div class="form-group col-md-6 Product">
            <label class="col-md-12" for="txtAreaDescription">Description<sup>*</sup></label>
            <textarea id="txtAreaDescription" name="description" class="form-control col-md-12" rows="3" placeholder="About Product"><%=description %></textarea>
            <div></div>
        </div>
        <div class="row"></div>
        <br />
        <div class="form-group col-md-10">
            <input type="button" class="btn btn-primary col-md-offset-4 col-md-3 col-sm-6" id="butAddNewProduct" value="Add New" />
            <input type="button" class="btn btn-primary col-md-offset-1 col-md-3 col-sm-6" id="butUpdateProduct" value="Update" />
        </div>
    </div>

    <div class="col-md-3">
        <br />
        <asp:GridView ID="gridView" BackColor="#d6d6d6" runat="server" AutoGenerateColumns="false">
            <Columns>
                <asp:TemplateField>
                    <ItemTemplate>
                        <asp:CheckBox class="Product" ID="chkGridView" name="productCategoryId" runat="server" />
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:BoundField DataField="productCategoryId" ItemStyle-CssClass="categoryId" HeaderText="ID" />
                <asp:BoundField DataField="categoryName" HeaderText="Category" />
            </Columns>
        </asp:GridView>
        <br/>
        <img src="<%=picture%>" id="imgPrev" height="120" width="100"/>
    </div>

</asp:Content>

