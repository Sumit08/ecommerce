﻿<%@ Page Title="" Language="C#" MasterPageFile="~/UserDetail.Master" AutoEventWireup="true" CodeBehind="AfterOrder.aspx.cs" Inherits="ECommerce.AfterOrder" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="col-md-2">
    </div>
    <div class="col-md-8">
        <div class="profile-content" style="min-height: 200px; margin-top: 40px; margin-bottom: 40px">
            <div class="col-md-12">
                <label class="col-md-offset-5" style="font-size: 2em">THANK YOU.</label>
            </div>
            <div class="col-md-12" style="margin-top:20px;">
                <p class="col-md-offset-4">YOUR ORDER HAS BEEN PLACED SUCCESSFULLY.</p>
            </div>
            <div class="col-md-12" style="margin-top:30px;">
                <input type="button" id="btnContinueShopping" class="btn btn-primary col-md-offset-5" value="CONTINUE SHOPPING" />
            </div>

        </div>
    </div>
</asp:Content>
