﻿<%@ Page Title="" Language="C#" MasterPageFile="~/UserDetail.Master" AutoEventWireup="true" CodeBehind="ViewCart.aspx.cs" Inherits="ECommerce.ViewCart" EnableEventValidation="false" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="col-md-2">
    </div>
    <div class="col-md-8">
        <div class="profile-content" style="min-height: 200px; margin-top: 40px; margin-bottom: 40px">
            <div class="row">
                <div class="col-md-12">
                    <h3>My Cart
                    </h3>
                    <hr />
                </div>
                <div class="col-md-12" style="background-color: lightgray">
                    <label class="col-md-7">ITEM</label>
                    <label class="col-md-1">QUANTITY</label>
                    <label class="col-md-1 col-md-offset-1">UNIT PRICE</label>
                    <label class="col-md-1 col-md-offset-1">UNIT TOTAL</label>
                </div>
                <div class="col-md-12">
                    <hr />
                </div>
                <asp:ListView ID="listViewCartProduct" runat="server">
                    <ItemTemplate>
                        <div class="thumbnail">
                            <div class="row">
                                <div class="col-md-7">
                                    <a href="ProductDetail.aspx?productID=<%#: Eval("ProductId") %>">
                                        <img class="col-md-4" src='<%#:"images/uplaodProductImages/"+Eval("picture") %>' alt="img" height="150" width="80" />
                                    </a>
                                    <div class="col-md-8">
                                        <div class="col-md-12">
                                            <label id="paraName" class="col-md-12"><%# Eval("Name") %></label>
                                        </div>
                                        <div class="col-md-12">
                                            <p class="col-md-12"><%# Eval("Descriptions") %></p>
                                        </div>
                                        <div class="col-md-12 row">
                                            <p id="paraCartInstock" class="col-md-11 col-md-offset-1">InStock:<%# Eval("InStock") %></p>
                                        </div>
                                        <div class="row">
                                            <input type="hidden" id="hiddenProductId" value="<%# Eval("ProductId") %>" />
                                        </div>
                                        <div class="col-md-2 col-md-offset-8" runat="server">
                                            <asp:Button runat="server" ID="btnRemoveCartProduct" ClientIDMode="Static" Text="REMOVE" class="btn btn-danger " CommandArgument='<%# Eval("ProductId") %>' OnCommand="btnRemoveCartProduct_Command" />
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-1">
                                    <%-- <input type="number" runat="server" id="numCartQuantity" value="1" min="1" max="<%# Eval("InStock") %>" />--%>
                                    <asp:TextBox class="col-md-2 form-control" ID="textCartQuantity" CssClass="txtQuantity" runat="server" Text='<%# Eval("ProductQuantity") %>'/>
                                </div>
                                <div class="col-md-1 col-md-offset-1 row">
                                    <p id="price">Rs. <%#: string.Format("{0:0.00}", Eval("UnitPrice")) %> </p>
                                </div>
                                <div class="col-md-1 col-md-offset-1 row">
                                    <p id="unitTotal" class="para">Rs. <%#: string.Format("{0:0.00}", Eval("UnitPrice")) %> </p>
                                </div>
                            </div>
                        </div>
                    </ItemTemplate>
                </asp:ListView>
                <div class="col-md-12" style="background-color: lightgray">
                    <p class="col-md-3 col-md-offset-9" id="payableAmount">Amount Payable: Rs. <span id="spanTotalAmount" runat="server" clientidmode="static"></span></p>
                </div>
                <div class="col-md-12" style="margin-top: 10px">
                    <input type="button" class="col-md-2 btn btn-primary col-md-offset-10" id="btnCartBuy" value='BUY' />
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-2">
    </div>

    <%if (((BussinessLogic.SessionValue)Session["User"]) != null)
        { %>
    <!-- placeOrder Modal -->
    <div class="modal fade" id="cartModal" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4>Address</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12 vertical-center">
                            <div class="col-md-12">
                                <h4 class="modal-title col-md-6">Billing Address</h4>
                                <h4 class="modal-title col-md-6">Delivery Address</h4>
                            </div>
                            <br />
                            <div class="col-md-6">
                                <div class="form-group has-feedback col-md-12">
                                    <input type="text" class="form-control" placeholder="Address"
                                        id="txtFirstName" name="firstName" value="<%=((BussinessLogic.SessionValue)Session["User"]).firstName %>" />
                                    <div></div>
                                </div>
                                <div class="form-group has-feedback col-md-12">
                                    <input type="text" class="form-control" placeholder="Address"
                                        id="txtLastName" name="lastName" value="<%=((BussinessLogic.SessionValue)Session["User"]).lastName %>" />
                                    <div></div>
                                </div>
                                <div class="form-group has-feedback col-md-12">
                                    <input type="text" class="form-control" placeholder="Address"
                                        id="txtAddress" name="addresses" value="<%=((BussinessLogic.SessionValue)Session["User"]).addresses %>" />
                                    <div></div>
                                </div>
                                <div class="form-group has-feedback col-md-12">
                                    <input type="text" class="form-control" placeholder="City" id="txtCity" name="city" value="<%=((BussinessLogic.SessionValue)Session["User"]).city %>" />
                                    <div></div>
                                </div>
                                <div class="form-group has-feedback col-md-12">
                                    <input type="text" class="form-control" placeholder="Postal code" id="txtPostalCode" name="postalCode" value="<%=((BussinessLogic.SessionValue)Session["User"]).postalCode %>" />
                                    <div></div>
                                </div>
                                <div class="form-group has-feedback col-md-12">
                                    <input type="text" class="form-control" placeholder="Mobile number" id="txtMobileNumber" name="mobileNumber" value="<%=((BussinessLogic.SessionValue)Session["User"]).mobileNumber %>" />
                                    <div></div>
                                </div>
                                <div class="form-group has-feedback col-md-12">
                                    <input type="checkbox" id="chkCartAddress" />Shipping address is same?
                                    <div></div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group has-feedback col-md-12 Address">
                                    <input type="text" class="form-control " placeholder="First name"
                                        id="txtShipFirstName" name="firstName" />
                                    <div></div>
                                </div>
                                <div class="form-group has-feedback col-md-12 Address">
                                    <input type="text" class="form-control " placeholder="Last name"
                                        id="txtShipLastName" name="lastName" />
                                    <div></div>
                                </div>
                                <div class="form-group has-feedback col-md-12 Address">
                                    <input type="text" class="form-control " placeholder="Address"
                                        id="txtShipAddress" name="addresses" />
                                    <div></div>
                                </div>
                                <div class="form-group has-feedback col-md-12 Address">
                                    <input type="text" class="form-control" placeholder="City" id="txtShipCity" name="city" />
                                    <div></div>
                                </div>
                                <div class="form-group has-feedback col-md-12 Address">
                                    <input type="text" class="form-control" placeholder="Postal code" id="txtShipPostalCode" name="postalCode" />
                                    <div></div>
                                </div>
                                <div class="form-group has-feedback col-md-12 Address">
                                    <input type="text" class="form-control" placeholder="Mobile number" id="txtShipMobileNumber" name="deliveryContact" />
                                    <div></div>
                                </div>
                                <div>
                                    <input type="button" id="btnCartModalSubmit" class="btn btn-success col-md-offset-8" value="Submit" />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <%}%>
</asp:Content>
