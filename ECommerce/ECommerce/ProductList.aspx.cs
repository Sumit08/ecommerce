﻿/*****************************************************************
*Created By: Sumit
*Created On: 17 Mar 2016
*Modified By:
*Modified On:
*Purpose: Lists the product based on the selected category.
******************************************************************/

using BussinessLogic;
using System;
using System.Web.UI.WebControls;

namespace ECommerce
{
    public partial class ProductList : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            getProduct();
        }

        /// <summary>
        /// Purpose: Calls getProductList method of UserBll class of BussinessLogic layer and list the products based on category.
        /// </summary>
        public void getProduct()
        {
            int categoryId;
            UserBll userBll = new UserBll();
            try
            {
                int.TryParse(Request.QueryString["CategoryID"], out categoryId);
                listProduct.DataSource = userBll.getProductList(categoryId);
                listProduct.DataBind();
            }
            catch (Exception ex)
            {
                Errors error = new Errors();
                error.LogError(ex);
            }
        }

        protected void listProduct_PagePropertiesChanging(object sender, System.Web.UI.WebControls.PagePropertiesChangingEventArgs e)
        {
            (listProduct.FindControl("DataPager1") as DataPager).SetPageProperties(e.StartRowIndex, e.MaximumRows, false);
            this.getProduct();
        }
    }
}