﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;

namespace ECommerce
{
    public class Errors
    {
        public void LogError(Exception ex)
        {
            string logFile = "ErrorLog_" + DateTime.Now.ToString("yyyy-MM-dd") + ".txt";
            StringBuilder message = new StringBuilder();
            message.Append("-----------------------------------------------------------");
            message.Append(Environment.NewLine);
            message.Append(string.Format("Time: {0}", DateTime.Now.ToString("dd/MM/yyyy hh:mm:ss tt")));
            message.Append(Environment.NewLine);
            message.Append("-----------------------------------------------------------");
            message.Append(Environment.NewLine);
            message.Append(string.Format("Message: {0}", ex.Message));
            message.Append(Environment.NewLine);
            message.Append(string.Format("StackTrace: {0}", ex.StackTrace));
            message.Append(Environment.NewLine);
            message.Append(string.Format("Source: {0}", ex.Source));
            message.Append(Environment.NewLine);
            message.Append(string.Format("TargetSite: {0}", ex.TargetSite.ToString()));
            message.Append(Environment.NewLine);
            message.Append("-----------------------------------------------------------");
            message.Append(Environment.NewLine);

            string path = System.Web.Configuration.WebConfigurationManager.AppSettings["errorLogPath"].ToString();
            path += logFile;
            if (!File.Exists(path))
            {
                FileStream file = File.Create(path);
                file.Close();
            }
            using (StreamWriter writer = new StreamWriter(path, true))
            {
                writer.WriteLine(message);
                writer.Close();
            }
        }
    }
}