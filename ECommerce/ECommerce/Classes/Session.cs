﻿/***********************************************************
*Created By: sumit
*Created On: 20th feb 2016
*MOdified By:
*Modified On:
*Purpose: To check the status of session.
************************************************************/

using System.Web;

namespace ECommerce.Classes
{
    public static class SessionHelper
    {
        private const string user = "User";
        public static BussinessLogic.SessionValue User
        {
            get
            {
                return (BussinessLogic.SessionValue)HttpContext.Current.Session[user] ?? null;
            }
            set
            {
                HttpContext.Current.Session[user] = value;
            }
        }

        //private const string supplier = "Supplier";
        //public static EntityModel.User Supplier
        //{
        //    get
        //    {
        //        return (EntityModel.User)HttpContext.Current.Session[supplier] ?? null;
        //    }
        //    set
        //    {
        //        HttpContext.Current.Session[supplier] = value;
        //    }
        //}
    }
}