﻿/*****************************************************************************
*Created By: Sumit
*Created On: 17 Mar 2016 
*Modified By:
*Modified On:
*Purpose: Seraches the item based on the search key word and lists the product.
*******************************************************************************/

using BussinessLogic;
using System;

namespace ECommerce
{
    public partial class searchedProduct : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            searchItem();
        }

        /// <summary>
        /// Purpose: Calls searchItem method of UserBll Class of BusinessLogic layer. 
        /// </summary>
        public void searchItem()
        {

            UserBll userBll = new UserBll();
            try
            {
                string searchWord = Request.QueryString["search"];
                var items = userBll.searchItem(searchWord);
                listSearchedProduct.DataSource = items;
                listSearchedProduct.DataBind();
            }
            catch (Exception ex)
            {
                Errors error = new Errors();
                error.LogError(ex);
            }
        }
    }
}