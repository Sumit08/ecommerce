﻿/***************************************************************************
*Created By: Sumit
*Created On: 14 feb 2016
*Modified By:
*Modified On:
*Purpose: Functionality required to dessign master page related to suplier.
****************************************************************************/

using System;

namespace ECommerce
{
    public partial class Supplier : System.Web.UI.MasterPage
    {
        /// <summary>
        /// Purpose: Fetching current year for footer. 
        /// </summary>
        protected void Page_Load(object sender, EventArgs e)
        {
            footerYear.InnerText = DateTime.Now.Year.ToString();

            //ddlSupplier.Style.Add("display", "none");
        }

        /// <summary>
        /// Purpose: Passes the search word through query string to searched product page.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnSearch_ServerClick(object sender, EventArgs e)
        {
            string searchWord = txtSearchItem.Value;
            if (searchWord != "")
            {
                Response.Redirect("searchedProduct.aspx?search=" + searchWord, true);
            }
        }
    }
}