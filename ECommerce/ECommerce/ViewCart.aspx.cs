﻿/*********************************************************************************
*Created By: Sumit
*Created On: 15 Mar 2016
*Modified By:
*Modified On:
*Purpose: Calling the cart related methods of BussinessLogic layer.
***********************************************************************************/

using BussinessLogic;
using ECommerce.Classes;
using EntityModel;
using System;
using System.Collections.Generic;
using System.Data;
using System.Web.Services;

namespace ECommerce
{
    public partial class ViewCart : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (SessionHelper.User == null)
            {
                Response.Redirect("HomePage.aspx", true);
            }
            bindCartProduct();
        }

        /// <summary>
        /// Purpose: Binds the listview to display the product in cart.
        /// </summary>
        public void bindCartProduct()
        {
            UserBll userBll = new UserBll();
            decimal totalAmount = 0;
            try
            {
                if (SessionHelper.User != null)
                {
                    var product = userBll.cartProductDetail(SessionHelper.User.userId);
                    listViewCartProduct.DataSource = product;
                    listViewCartProduct.DataBind();
                    foreach (DataRow row in product.Rows)
                    {
                        totalAmount += (row.Field<decimal>("UnitPrice"));
                    }
                    spanTotalAmount.InnerText = (string.Format("{0:0.00}", totalAmount)).ToString();
                }
            }
            catch (Exception ex)
            {
                Errors error = new Errors();
                error.LogError(ex);
            }
        }

        /// <summary>
        /// Purpose: Get the productId on click of remove button and call deleteCartProduct method of UserBll class. 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnRemoveCartProduct_Command(object sender, System.Web.UI.WebControls.CommandEventArgs e)
        {
            int productId;
            UserBll userBll = new UserBll();
            try
            {
                int.TryParse(e.CommandArgument.ToString(), out productId);
                if (userBll.deleteCartProduct(productId))
                {
                    Response.Redirect("ViewCart.aspx", true);
                }
            }
            catch (Exception ex)
            {
                Errors error = new Errors();
                error.LogError(ex);
            }
        }

        /// <summary>
        /// Purpose: Gets all required information to place cart order and calls placeCartOrder method of UserBll class. 
        /// </summary>
        /// <param name="address"></param>
        /// <returns></returns>
        [WebMethod]
        public static int saveCartOrder(Address address)
        {
            try
            {
                int userId = SessionHelper.User.userId;
                UserBll userBll = new UserBll();
                Order order = new Order();
                List<Product> productList = new List<Product>();

                var productTable = userBll.cartProductDetail(userId);
                order.orderDate = DateTime.Now;
                order.orderStatus = "success";
                order.usersId = userId;
                address.name = (SessionHelper.User.firstName) + " " + (SessionHelper.User.lastName);

                foreach (DataRow row in productTable.Rows)
                {
                    Product product = new Product()
                    {
                        productId = row.Field<int>("ProductId"),
                        inStock = row.Field<int>("InStock"),
                        cartProductQuantity=row.Field<int>("ProductQuantity")
                    };
                    productList.Add(product);
                }
                if (userBll.placeCartOrder(address, order, productList))
                {
                    return (int)UserMessage.userMessage.success;
                }
                else
                {
                    return (int)UserMessage.userMessage.failure;
                }
            }
            catch (Exception ex)
            {
                Errors error = new Errors();
                error.LogError(ex);
                return (int)UserMessage.userMessage.exception;
            }
        }

        /// <summary>
        /// Purpose: update the cart product quantity on change of quantity on cart.
        /// </summary>
        /// <param name="productId"></param>
        /// <param name="quantity"></param>
        /// <returns></returns>
        [WebMethod]
        public static int updateCart(int productId,int quantity)
        {
            UserBll userBll = new UserBll();
            try
            {
                int userId = SessionHelper.User.userId;
                if(userBll.updateCartQuantity(productId,quantity,userId))
                {
                    return (int)UserMessage.userMessage.success;
                }
                else
                {
                    return (int)UserMessage.userMessage.failure;
                }

            }
            catch(Exception ex)
            {
                Errors error = new Errors();
                error.LogError(ex);
                return (int)UserMessage.userMessage.exception;
            }
        }
    }
}