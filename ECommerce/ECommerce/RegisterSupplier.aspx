﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Supplier.Master" AutoEventWireup="true" CodeBehind="RegisterSupplier.aspx.cs" Inherits="ECommerce.RegisterSupplier" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="class-col-md-12">
        <h4>
            <label class="col-md-offset-1" style="color:red">Register yourself for First Login. </label>
        </h4>
    </div>
    <div class="col-md-3">
    </div>

    <%-- Register area--%>
    <div class="col-md-6 Register">
        <div class="form-group  col-md-6  User">
            <label class="col-md-12 " for="txtFirstName">First Name<sup>*</sup></label>
            <input type="text" id="txtFirstName" name="firstName" class="form-control col-md-12" maxlength="25" placeholder="First name" />
            <div class="col-md-12"></div>
        </div>
        <div class="form-group col-md-6  User">
            <label class="col-md-12 " for="txtLastName">Last Name<sup>*</sup></label>
            <input type="text" id="txtLastName" name="lastName" class="form-control col-md-12" maxlength="25" placeholder="Last name" />
            <div class="col-md-12"></div>
        </div>
        <div class="form-group col-md-6 User">
            <label class="col-md-12 " for="txtEmailId">Email Id<sup>*</sup></label>
            <input type="text" id="txtEmailId" name="email" class="form-control col-md-12 " maxlength="100" placeholder="Email" />
            <div class="col-md-12"></div>
        </div>
        <div class="form-group col-md-6 User">
            <label class="col-md-12 " for="pwdPassword">Password<sup>*</sup></label>
            <input type="password" id="pwdPassword" name="password" class="form-control col-md-12 " placeholder="Password" maxlength="25" />
            <div class="col-md-12"></div>
        </div>
        <div class="form-group col-md-6 Address">
            <label class="col-md-12 " for="txtAddress">Address<sup>*</sup></label>
            <input type="text" id="txtAddress" name="addresses" class="form-control col-md-12" placeholder="Address" maxlength="100" />
            <div class="col-md-12"></div>
        </div>
        <div class="form-group col-md-6 Address">
            <label class="col-md-12 " for="txtCity">City<sup>*</sup></label>
            <input type="text" id="txtCity" name="city" class="form-control col-md-12" placeholder="City name" maxlength="100" />
            <div class="col-md-12"></div>
        </div>
        <div class="form-group col-md-6">
            <label class="col-md-12 " for="listCountry">Country<sup>*</sup></label>
            <select id="listCountry" class="form-control col-md-12">
            </select>
            <div class="col-md-12"></div>
        </div>
        <div class="form-group col-md-6 Address">
            <label class="col-md-12 " for="listState">State<sup>*</sup></label>
            <select id="listState" name="stateId" class="form-control col-md-12">
            </select>
            <div class="col-md-12"></div>
        </div>
        <div class="form-group col-md-6 Address">
            <label class="col-md-12 " for="txtZipCode">Zip Code<sup>*</sup></label>
            <input type="text" id="txtZipCode" name="postalCode" class="form-control col-md-12" placeholder="Enter zip code" maxlength="6" />
            <div class="col-md-12"></div>
        </div>
        <div class="form-group col-md-6 User">
            <label class="col-md-12 " for="txtMobile">Mobile Number<sup>*</sup></label>
            <input type="text" id="txtMobile" name="mobileNumber" class="form-control col-md-12" maxlength="15" placeholder="Mobile number" />
            <div class="col-md-12"></div>
        </div>
        <div class="form-group  col-md-offset-5 col-md-10">
            <input type="button" class="btn btn-primary col-md-3 col-sm-6" id="butSubmit" value="Register" />
        </div>
    </div>

</asp:Content>
