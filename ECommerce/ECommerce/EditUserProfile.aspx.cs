﻿/*******************************************************************************************
*Created By: Sumit
*Created On: 15 Mar 2016
*Modified By: 
*Modified On:
*Purpose: calling the methods of Business logic to update user Details..  
*********************************************************************************************/

using BussinessLogic;
using ECommerce.Classes;
using EntityModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;

namespace ECommerce
{
    public partial class EditUserProfile : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (SessionHelper.User == null)
            {
                Response.Redirect("HomePage.aspx", true);
            }
        }

        /// <summary>
        /// Purpose: Calls editProfile method of UserBll class of BussinessLogic layer.
        /// </summary>
        /// <param name="address"></param>
        /// <param name="user"></param>
        /// <returns></returns>
        [WebMethod]
        public static int profile(Address address, User user)
        {
            UserBll userBll = new UserBll();
            try
            {
                user.usersId = SessionHelper.User.userId;
                if (userBll.editProfile(address, user))
                {
                    SessionHelper.User = null;
                    SessionValue sessionValue = new SessionValue();
                    var userDetail = sessionValue.getUserDetail(user.email);
                    HttpContext.Current.Session.Add("User", userDetail);
                    return (int)UserMessage.userMessage.success; ;
                }
                else
                {
                    return (int)UserMessage.userMessage.failure; ;
                }
            }
            catch (Exception ex)
            {
                Errors error = new Errors();
                error.LogError(ex);
                return (int)UserMessage.userMessage.exception;
            }
        }


        /// <summary>
        /// Purpose: Method for binding country.
        /// </summary>
        /// <returns></returns>
        [WebMethod]
        public static List<KeyValuePair<int, string>> countryList()
        {
            SuplierBll supplierBll = new SuplierBll();
            return supplierBll.getCountry().ToList();
        }

        /// <summary>
        /// Purpose: Method for binding state.
        /// </summary>
        /// <param name="country"></param>
        /// <returns></returns>
        [WebMethod]
        public static List<KeyValuePair<int, string>> stateList(Country country)
        {
            SuplierBll supplierBll = new SuplierBll();
            return supplierBll.getState(country.countryId).ToList();

        }
    }
}