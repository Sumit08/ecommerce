﻿/*******************************************************************************************
*Created By: Sumit
*Created On: 15 Mar 2016
*Modified By: 
*Modified On:
*Purpose: Calling the methods of Business logic to display the product detail, adding to cart.  
*********************************************************************************************/

using BussinessLogic;
using ECommerce.Classes;
using EntityModel;
using System;
using System.Configuration;
using System.Web.Services;

namespace ECommerce
{
    public partial class ProductDetail : System.Web.UI.Page
    {
        protected string productName { get; set; }
        protected float unitPrice { get; set; }
        protected int inStock { get; set; }
        protected string description { get; set; }
        protected string picture { get; set; }
        protected float unitWeight { get; set; }

        protected void Page_Load(object sender, EventArgs e)
        {

            int productId;
            try
            {
                int.TryParse(Request.QueryString["productId"], out productId);
            }
            catch (Exception ex)
            {
                Errors error = new Errors();
                error.LogError(ex);
            }
            getProductDetail();

        }

        /// <summary>
        /// Purpose: Displaying the product detail of selected product.
        /// </summary>
        protected void getProductDetail()
        {
            int productId;
            SuplierBll suplierBll = new SuplierBll();
            try
            {
                int.TryParse(Request.QueryString["productId"], out productId);
                Product product = suplierBll.bindProductFields(productId);
                productName = product.productName;
                unitPrice = product.unitPrice;
                unitWeight = product.unitWeight;
                inStock = product.inStock;
                description = product.description;
                picture = (ConfigurationManager.AppSettings["imgPath"].ToString() + product.picture);
            }
            catch (Exception ex)
            {
                Errors error = new Errors();
                error.LogError(ex);
            }
        }

        /// <summary>
        /// Purpose: checking session and on true fill address fields of order modal.
        /// </summary>
        /// <returns></returns>
        [WebMethod]
        public static bool checkSession()
        {
            if (SessionHelper.User == null)
            {
                return false;
            }
            else
            {
                return true;
            }
        }

        /// <summary>
        /// Purpose: Call saveOrderBll method of UserBll class of bll layer.
        /// </summary>
        /// <param name="address"></param>
        /// <param name="orderDetail"></param>
        /// <returns></returns>
        [WebMethod]
        public static int saveOrder(Address address, OrderDetail orderDetail)
        {
            Order order = new Order();
            User user = new User();
            UserBll userBll = new UserBll();
            try
            {
                order.usersId = SessionHelper.User.userId;
                order.orderDate = DateTime.Now;
                order.orderStatus = "Success";
                address.name = (SessionHelper.User.firstName) + "" + (SessionHelper.User.lastName);
                if (userBll.saveOrderBll(address, orderDetail, order))
                {
                    return (int)UserMessage.userMessage.success;
                }
                else
                {
                    return (int)UserMessage.userMessage.failure;
                }
            }
            catch (Exception ex)
            {
                Errors error = new Errors();
                error.LogError(ex);
                return (int)UserMessage.userMessage.exception;
            }

        }

        /// <summary>
        /// Purpose: Check session for adding product to cart.
        /// </summary>
        [WebMethod]
        public static bool checkSessionCart(Product product)
        {
            if (SessionHelper.User == null)
            {
                return false;
            }
            else
            {
                return true;
            }

        }

        /// <summary>
        /// Purpose: Add product ID to cart and returns count of product in cart.
        /// </summary>
        /// <param name="product"></param>
        /// <returns></returns>
        [WebMethod]
        public static int countAddCart(Product product)
        {
            try
            {
                UserBll userBll = new UserBll();
                var userId = SessionHelper.User.userId;
                var productId = product.productId;
                int returnValue = userBll.addCart(userId, productId);
                if (returnValue != -1)
                {
                    return (int)UserMessage.userMessage.success;
                }
                else
                {
                    return (int)UserMessage.userMessage.failure;
                }
            }
            catch (Exception ex)
            {
                Errors error = new Errors();
                error.LogError(ex);
                return (int)UserMessage.userMessage.exception;
            }
        }
    }
}