﻿<%@ Page Title="" Language="C#" MasterPageFile="~/UserDetail.Master" AutoEventWireup="true" CodeBehind="Categories.aspx.cs" Inherits="ECommerce.Categories" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <div class="col-md-1">
    </div>

    <div class="col-md-10">
        <div class="profile-content" style="margin-top: 30px; margin-bottom:50px; min-height: 700px">
            <h2>
                <label class="col-md-12" style="background-color: lightgray">Category</label>
            </h2>
            <asp:ListView ID="listViewCategory" runat="server" GroupPlaceholderID="3">
                <GroupTemplate>
                    <tr>
                        <td id="itemPlaceHolder" runat="server"></td>
                    </tr>
                </GroupTemplate>
                <ItemTemplate>
                    <div class="col-md-3" style="margin-top: 40px">
                        <a href="ProductList.aspx?CategoryId=<%# Eval("ProductCategoryId") %>" class="img-thumbnail">
                            <span class="col-md-12"><%#: Eval("CategoryName") %></span>
                            <center><img src="<%#:"images/uplaodProductImages/"+Eval("CategoryImage") %>" alt="image" height="200" width="220"/></center>
                        </a>
                    </div>
                </ItemTemplate>
            </asp:ListView>
        </div>
    </div>

</asp:Content>
