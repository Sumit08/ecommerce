﻿<%@ Page Title="" Language="C#" MasterPageFile="~/UserDetail.Master" AutoEventWireup="true" CodeBehind="OrdersPage.aspx.cs" Inherits="ECommerce.OrdersPage" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <div class="col-md-2">
    </div>

    <div class="col-md-8">
        <div class="profile-content" style="margin-bottom:20px">
            <div class="row">
                <div class="col-md-12">
                    <h3>My Orders
                    </h3>
                    <hr />
                </div>
                <div class="col-md-12">
                    <label class="col-md-2 col-md-offset-1">Order Id</label>
                    <label class="col-md-3">Ordered Date</label>
                    <label class="col-md-2">Order Status</label>
                    <hr class="col-md-12" />
                </div>
                <asp:ListView ID="listViewOrders" runat="server">
                    <ItemTemplate>
                        <div class="row">
                            <div class="col-md-2 col-md-offset-1">
                                <p class="col-md-12">OD20160<%#  Eval("OrdersId") %></p>
                            </div>
                            <div class="col-md-3">
                                <p class="col-md-12"><%#  Eval("OrderDate") %></p>
                            </div>
                            <div class="col-md-2">
                                <p class="col-md-12"><%#  Eval("OrderStatus") %></p>
                            </div>
                        </div>
                        <a href="OrderDetailPage.aspx?OrdersID=<%# Eval("OrdersId")%>">
                            <input type="button" runat="server" class="col-md-2 btn btn-primary col-md-offset-9" id="btnOrderDetail"
                                value="viewDetail" />
                            <div class="col-md-12">
                                <hr />
                            </div>
                        </a>
                    </ItemTemplate>
                </asp:ListView>
            </div>
        </div>
    </div>

</asp:Content>
