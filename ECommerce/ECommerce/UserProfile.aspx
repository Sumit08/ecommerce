﻿<%@ Page Title="" Language="C#" MasterPageFile="~/UserDetail.Master" AutoEventWireup="true" CodeBehind="UserProfile.aspx.cs" Inherits="ECommerce.UserProfile" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="container-fluid">
        <div class="row profile">
            <div class="col-md-3">
                <div class="profile-sidebar">
                    <!-- SIDEBAR USERPIC -->
                    <div class="profile-userpic">
                        <img src="http://www.psdgraphics.com/file/user-icon.jpg" class="img-responsive" alt="" />
                    </div>
                    <!-- END SIDEBAR USERPIC -->
                    <!-- SIDEBAR USER TITLE -->
                    <%if ((BussinessLogic.SessionValue)Session["User"] != null)
                        { %>
                    <div class="profile-usertitle">
                        <div class="profile-usertitle-name">
                            <%=((BussinessLogic.SessionValue)Session["User"]).firstName%> <%=((BussinessLogic.SessionValue)Session["User"]).lastName%>
                        </div>
                        <div class="profile-usertitle-job">
                            <%=((BussinessLogic.SessionValue)Session["User"]).email%>
                        </div>
                    </div>
                    <%} %>
                    <!-- END SIDEBAR USER TITLE -->
                    <!-- SIDEBAR MENU -->
                    <div class="profile-usermenu">
                        <ul class="nav">
                            <li class="active">
                                <a href="UserProfile.aspx">
                                    <i class="glyphicon glyphicon-user"></i>
                                    Overview </a>
                            </li>
                            <li>
                                <a href="EditUserProfile.aspx">
                                    <i class="glyphicon glyphicon-edit"></i>
                                    Edit Profile </a>
                            </li>
                            <li>
                                <a href="javaScript:void(0);" id="userProfileLogout">
                                    <i class="glyphicon glyphicon-log-out"></i>
                                    Logout </a>
                            </li>
                        </ul>
                    </div>
                    <!-- END MENU -->
                </div>
            </div>

            <%--profile section--%>
            <%if ((BussinessLogic.SessionValue)Session["User"] != null)
                { %>
            <div class="col-md-8">
                <div class="profile-content">
                    <div class="row">
                        <div class="col-md-12">
                            <h1>Profile
                            </h1>
                            <hr />
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <%--Personal Information--%>
                            <table class="table borderless">
                                <!-- first name -->
                                <tr>
                                    <th class="col-md-3">
                                        <p>Personal Information</p>
                                    </th>
                                    <td class="col-md-3">
                                        <p>First name:</p>
                                    </td>
                                    <td class="col-md-6">
                                        <p><%=((BussinessLogic.SessionValue)Session["User"]).firstName%></p>
                                    </td>
                                </tr>

                                <!-- last name -->
                                <tr>
                                    <td class="col-md-3"></td>
                                    <td class="col-md-3">
                                        <p>Last name:</p>
                                    </td>
                                    <td class="col-md-6">
                                        <p><%=((BussinessLogic.SessionValue)Session["User"]).lastName%></p>
                                    </td>
                                </tr>
                            </table>
                            <div class="col-md-12">
                                <hr />
                            </div>
                            <%--Address--%>
                            <table class="table borderless">
                                <!-- Address -->
                                <tr>
                                    <th class="col-md-3">
                                        <p>Address:</p>
                                    </th>
                                    <td class="col-md-3">
                                        <p>Street:</p>
                                    </td>
                                    <td class="col-md-6">
                                        <p><%=((BussinessLogic.SessionValue)Session["User"]).addresses%></p>
                                    </td>
                                </tr>
                                <!-- city -->
                                <tr>
                                    <td class="col-md-3"></td>
                                    <td class="col-md-3">
                                        <p>City:</p>
                                    </td>
                                    <td class="col-md-6">
                                        <p><%=((BussinessLogic.SessionValue)Session["User"]).city%></p>
                                    </td>
                                </tr>
                                <%--state--%>
                                <tr>
                                    <td class="col-md-3"></td>
                                    <td class="col-md-3">
                                        <p>State:</p>
                                    </td>
                                    <td class="col-md-6">
                                        <p></p>
                                    </td>
                                </tr>
                                <%--country--%>
                                <tr>
                                    <td class="col-md-3"></td>
                                    <td class="col-md-3">
                                        <p>Country:</p>
                                    </td>
                                    <td class="col-md-6">
                                        <p></p>
                                    </td>
                                </tr>
                                <%--postal code--%>
                                <tr>
                                    <td class="col-md-3"></td>
                                    <td class="col-md-3">
                                        <p>Postal code:</p>
                                    </td>
                                    <td class="col-md-6">
                                        <p><%=((BussinessLogic.SessionValue)Session["User"]).postalCode%></p>
                                    </td>
                                </tr>
                            </table>
                            <div class="col-md-12">
                                <hr />
                            </div>
                            <%--Contact--%>
                            <table class="table borderless">
                                <!--mobile number-->
                                <tr>
                                    <th class="col-md-3">
                                        <p>Contact</p>
                                    </th>
                                    <td class="col-md-3">
                                        <p>Mobile number:</p>
                                    </td>
                                    <td class="col-md-6">
                                        <p><%=((BussinessLogic.SessionValue)Session["User"]).mobileNumber%></p>
                                    </td>
                                </tr>

                                <!-- last name -->
                                <tr>
                                    <td class="col-md-3"></td>
                                    <td class="col-md-3">
                                        <p>Email:</p>
                                    </td>
                                    <td class="col-md-6">
                                        <p><%=((BussinessLogic.SessionValue)Session["User"]).email%></p>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <%} %>
        </div>
    </div>
</asp:Content>
