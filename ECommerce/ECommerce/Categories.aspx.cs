﻿/***********************************************************
*Created By: Sumit
*Created On: 16 Mar 2016
*Modified By: 
*Modified On:
*Purpose: Lists the available category of items.
************************************************************/

using BussinessLogic;
using System;

namespace ECommerce
{
    public partial class Categories : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            listCategory();
        }

        /// <summary>
        /// Purpose: List all the categories of item available on page load.
        /// </summary>
        public void listCategory()
        {
            UserBll userBll = new UserBll();
            var categories = userBll.listCategory();
            listViewCategory.DataSource = categories;
            listViewCategory.DataBind();
        }
    }
}