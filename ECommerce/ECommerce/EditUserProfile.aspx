﻿<%@ Page Title="" Language="C#" MasterPageFile="~/UserDetail.Master" AutoEventWireup="true" CodeBehind="EditUserProfile.aspx.cs" Inherits="ECommerce.EditUserProfile" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <div class="container-fluid">
        <div class="row profile">
            <div class="col-md-3">
                <div class="profile-sidebar">
                    <!-- SIDEBAR USERPIC -->
                    <div class="profile-userpic">
                        <img src="http://www.psdgraphics.com/file/user-icon.jpg" class="img-responsive" alt="" />
                    </div>
                    <!-- END SIDEBAR USERPIC -->
                    <!-- SIDEBAR USER TITLE -->
                    <div class="profile-usertitle">
                        <div class="profile-usertitle-name">
                            <%=((BussinessLogic.SessionValue)Session["User"]).firstName %> <%=((BussinessLogic.SessionValue)Session["User"]).lastName %>
                        </div>
                        <div class="profile-usertitle-job">
                            <%=((BussinessLogic.SessionValue)Session["User"]).email %>
                        </div>
                    </div>
                    <!-- END SIDEBAR USER TITLE -->
                    <!-- SIDEBAR MENU -->
                    <div class="profile-usermenu">
                        <ul class="nav">
                            <li>
                                <a href="UserProfile.aspx">
                                    <i class="glyphicon glyphicon-user"></i>
                                    Overview </a>
                            </li>
                            <li class="active">
                                <a href="EditUserProfile.aspx">
                                    <i class="glyphicon glyphicon-edit"></i>
                                    Edit Profile </a>
                            </li>
                            <li>
                                <a href="javaScript:void(0);" id="userProfileLogout">
                                    <i class="glyphicon glyphicon-log-out"></i>
                                    Logout </a>
                            </li>
                        </ul>
                    </div>
                    <!-- END MENU -->
                </div>
            </div>

            <div class="col-md-8">
                <div class="profile-content">
                    <div class="col-md-12">
                        <h1>Edit Profile
                        </h1>
                        <hr />
                    </div>
                    <div class="col-md-9">
                        <div class="form-group  col-md-6  User">
                            <label class="col-md-12 col-md-offset-1" for="txtProfileFirstName">First Name</label>
                            <input type="text" id="txtProfileFirstName" name="firstName" class="form-control col-md-12 col-md-offset-1" maxlength="25" placeholder="First name" value="<%=((BussinessLogic.SessionValue)Session["User"]).firstName %>" />
                            <div></div>
                        </div>
                        <div class="form-group col-md-6  User">
                            <label class="col-md-12 col-md-offset-1" for="txtProfileLastName">Last Name</label>
                            <input type="text" id="txtProfileLastName" name="lastName" class="form-control col-md-12 col-md-offset-1" maxlength="25" placeholder="Last name" value="<%=((BussinessLogic.SessionValue)Session["User"]).lastName %>" />
                            <div></div>
                        </div>
                        <div class="form-group col-md-6 User">
                            <label class="col-md-12 col-md-offset-1" for="txtProfileEmailId">Email Id<sup>*</sup></label>
                            <input type="text" id="txtProfileEmailId" name="email" class="form-control col-md-12 col-md-offset-1" maxlength="100" placeholder="Email" value="<%=((BussinessLogic.SessionValue)Session["User"]).email %>" />
                            <div></div>
                        </div>
                        <div class="form-group col-md-6 User">
                            <label class="col-md-12 col-md-offset-1" for="pwdProfilePassword">Password<sup>*</sup></label>
                            <input type="password" id="pwdProfilePassword" name="password" class="form-control col-md-12 col-md-offset-1" placeholder="Password" maxlength="25" />
                            <div></div>
                        </div>
                        <div class="form-group col-md-6 Address">
                            <label class="col-md-12 col-md-offset-1" for="txtProfileAddress">Address</label>
                            <input type="text" id="txtProfileAddress" name="addresses" class="form-control col-md-12 col-md-offset-1" placeholder="Address" maxlength="100" value="<%=((BussinessLogic.SessionValue)Session["User"]).addresses %>" />
                            <div></div>
                        </div>
                        <div class="form-group col-md-6 Address">
                            <label class="col-md-12 col-md-offset-1" for="txtProfileCity">City</label>
                            <input type="text" id="txtProfileCity" name="city" class="form-control col-md-12 col-md-offset-1" placeholder="City name" maxlength="100" value="<%=((BussinessLogic.SessionValue)Session["User"]).city %>" />
                            <div></div>
                        </div>

                        <div class="form-group col-md-6">
                            <label class="col-md-12 col-md-offset-1" for="profileListCountry">Country</label>
                            <select id="profileListCountry" class="form-control col-md-12 col-md-offset-1">
                            </select>
                            <div class="col-md-12"></div>
                        </div>
                        <div class="form-group col-md-6 Address">
                            <label class="col-md-12 col-md-offset-1" for="profileListState">State</label>
                            <select id="profileListState" name="stateId" class="form-control col-md-12 col-md-offset-1">
                            </select>
                            <div class="col-md-12"></div>
                        </div>

                        <div class="form-group col-md-6 Address">
                            <label class="col-md-12 col-md-offset-1" for="txtProfileZipCode">Postal Code</label>
                            <input type="text" id="txtProfileZipCode" name="postalCode" class="form-control col-md-12 col-md-offset-1" placeholder="Enter zip code" maxlength="6" value="<%=((BussinessLogic.SessionValue)Session["User"]).postalCode %>" />
                            <div></div>
                        </div>
                        <div class="form-group col-md-6 User">
                            <label class="col-md-12 col-md-offset-1" for="txtProfileMobile">Mobile Number</label>
                            <input type="text" id="txtProfileMobile" name="mobileNumber" class="form-control col-md-12 col-md-offset-1" maxlength="15" placeholder="Mobile number" value="<%=((BussinessLogic.SessionValue)Session["User"]).mobileNumber %>" />
                            <div></div>
                        </div>
                        <div class="form-group  col-md-offset-5 col-md-10">
                            <input type="button" class="btn btn-primary col-md-3 col-sm-6" id="btnSaveUserProfile" value="Save" />
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
