﻿/*******************************************************************************************
*Created By: Sumit
*Created On: 15 Mar 2016
*Modified By: 
*Modified On:
*Purpose: Calling the methods of Business logic to display the user orders.  
*********************************************************************************************/

using BussinessLogic;
using ECommerce.Classes;
using System;

namespace ECommerce
{
    public partial class OrdersPage : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (SessionHelper.User == null)
            {
                Response.Redirect("HomePage.aspx", true);
            }
            userOrders();
        }

        /// <summary>
        /// Purpose: Calls getOrder method of UserBll class of Business logic to display user order.
        /// </summary>
        public void userOrders()
        {
            UserBll userBll = new UserBll();
            try
            {
                listViewOrders.DataSource = userBll.getOrder(SessionHelper.User.userId);
                listViewOrders.DataBind();
            }
            catch (Exception ex)
            {
                Errors error = new Errors();
                error.LogError(ex);
            }
        }
    }
}