﻿/************************************************************************************
*Created By: Sumit
*Created On: 30 Mar 2016
*Modified By:
*Modified On:
*Purpose: Displaying successfull oder placing message and redirecting to home page.
*************************************************************************************/

using ECommerce.Classes;
using System;

namespace ECommerce
{
    public partial class AfterOrder : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (SessionHelper.User == null)
            {
                Response.Redirect("HomePage.aspx", true);
            }
        }
    }
}