﻿<%@ Page Title="" Language="C#" MasterPageFile="~/UserDetail.Master" AutoEventWireup="true" CodeBehind="ProductDetail.aspx.cs" Inherits="ECommerce.ProductDetail" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <div class="col-md-1">
    </div>
    <div class="col-md-10 profile-content" style="margin-top: 40px; margin-bottom: 30px; min-height: 350px">
        <div class="col-md-3 ">
            <img src="<%=picture %>" alt="image" height="360" width="290" />
        </div>
        <div class="col-md-6 col-md-offset-1">
            <h1>
                <label class="col-md-12"><%=productName %></label>
            </h1>
            <div class="col-md-12">
                <hr />
                <br />
                <br />
            </div>
            <p class="col-md-12"><%=description %></p>
            <p class="col-md-12" id="chkStockValue">In Stock: <%=inStock %></p>
            <p class="col-md-12">Rs. <%=unitPrice %></p>
            <div class="col-md-12">
                <br />
                <br />
                <hr />
            </div>
            <button id="btnBuyNow" class="btn btn-success col-md-3" type="button">BUY NOW</button>
            <button id="btnAddCart" class="btn btn-warning col-md-3 col-md-offset-1 " type="button"><span class="glyphicon glyphicon-shopping-cart"></span>ADD TO CART</button>
        </div>
    </div>

    <%if (((BussinessLogic.SessionValue)Session["User"]) != null)
        { %>
    <!-- placeOrder Modal -->
    <div class="modal fade" id="placeOrderModal" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4>Product Details</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div>
                                <p class="col-md-6">Unit Price:</p>
                                <p class="col-md-6" id="paraUnitPrice"><%=unitPrice %> Rs</p>
                            </div>
                            <div>
                                <p class="col-md-6">Unit Weight:</p>
                                <p class="col-md-6"><%=unitWeight %> Kg</p>
                            </div>
                            <div>
                                <p class="col-md-6">InStock:</p>
                                <p class="col-md-6" id="paraInStock"><%=inStock %></p>
                            </div>
                            <div class="OrderDetail">
                                <p class="col-md-6 ">Quantity:</p>
                                <input type="number" class="col-md-2" id="numQuantity" name="quantity" min="1" max="???" value="1" />
                                <div></div>
                            </div>
                            <div class="col-md-12">
                            </div>
                            <div>
                                <p class="col-md-6">Total Amount:</p>
                                <p class="col-md-6" id="paraTotalAmount"></p>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <hr />
                        </div>
                        <div class="col-md-12 vertical-center">
                            <div class="col-md-12">
                                <h4 class="modal-title col-md-6">Billing Address</h4>
                                <h4 class="modal-title col-md-6">Delivery Address</h4>
                            </div>
                            <br />
                            <div class="col-md-6">
                                <div class="form-group has-feedback col-md-12">
                                    <input type="text" class="form-control" placeholder="Address"
                                        id="txtFirstName" name="firstName" value="<%=((BussinessLogic.SessionValue)Session["User"]).firstName %>" />
                                    <div></div>
                                </div>
                                <div class="form-group has-feedback col-md-12">
                                    <input type="text" class="form-control" placeholder="Address"
                                        id="txtLastName" name="lastName" value="<%=((BussinessLogic.SessionValue)Session["User"]).lastName %>" />
                                    <div></div>
                                </div>
                                <div class="form-group has-feedback col-md-12">
                                    <input type="text" class="form-control" placeholder="Address"
                                        id="txtAddress" name="addresses" value="<%=((BussinessLogic.SessionValue)Session["User"]).addresses %>" />
                                    <div></div>
                                </div>
                                <div class="form-group has-feedback col-md-12">
                                    <input type="text" class="form-control" placeholder="City" id="txtCity" name="city" value="<%=((BussinessLogic.SessionValue)Session["User"]).city %>" />
                                    <div></div>
                                </div>
                                <div class="form-group has-feedback col-md-12">
                                    <input type="text" class="form-control" placeholder="Postal code" id="txtPostalCode" name="postalCode" value="<%=((BussinessLogic.SessionValue)Session["User"]).postalCode %>" />
                                    <div></div>
                                </div>
                                <div class="form-group has-feedback col-md-12">
                                    <input type="text" class="form-control" placeholder="Mobile number" id="txtMobileNumber" name="mobileNumber" value="<%=((BussinessLogic.SessionValue)Session["User"]).mobileNumber %>" />
                                    <div></div>
                                </div>
                                <div class="form-group has-feedback col-md-12">
                                    <input type="checkbox" id="chkAddress" />Shipping address is same?
                                    <div></div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group has-feedback col-md-12 Address">
                                    <input type="text" class="form-control " placeholder="First name"
                                        id="txtShipFirstName" name="name" />
                                    <div></div>
                                </div>
                                <div class="form-group has-feedback col-md-12 Address">
                                    <input type="text" class="form-control " placeholder="Last name"
                                        id="txtShipLastName" />
                                    <div></div>
                                </div>
                                <div class="form-group has-feedback col-md-12 Address">
                                    <input type="text" class="form-control " placeholder="Address"
                                        id="txtShipAddress" name="addresses" />
                                    <div></div>
                                </div>
                                <div class="form-group has-feedback col-md-12 Address">
                                    <input type="text" class="form-control" placeholder="City" id="txtShipCity" name="city" />
                                    <div></div>
                                </div>
                                <div class="form-group has-feedback col-md-12 Address">
                                    <input type="text" class="form-control" placeholder="Postal code" id="txtShipPostalCode" name="postalCode" />
                                    <div></div>
                                </div>
                                <div class="form-group has-feedback col-md-12 Address">
                                    <input type="text" class="form-control" placeholder="Mobile number" id="txtShipMobileNumber" name="deliveryContact" />
                                    <div></div>
                                </div>
                                <div>
                                    <input type="button" id="btnOrderModalSubmit" class="btn btn-success col-md-offset-8" value="Submit" />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <%}%>
</asp:Content>
