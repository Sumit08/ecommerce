﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EntityModel
{
    public class State
    {
        public int stateId { get; set; }
        public string stateName { get; set; }
        public int countryId { get; set; }
    }
}
