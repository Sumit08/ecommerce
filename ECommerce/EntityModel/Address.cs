﻿/**************************************************
* Created By: Sumit
* Created On: 4th feb 2016
* Modified By:
* Modified On:
* Purpose: To set the Address details.
**************************************************/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EntityModel
{
    public class Address
    {
        public string addresses { get; set; }
        public string city { get; set; }
        public string postalCode { get; set; }
        public int usersId { get; set; }
        public int stateId { get; set; }
        public int addressId { get; set; }
        public string name { get; set; }
        public string deliveryContact { get; set; }
    }
}
