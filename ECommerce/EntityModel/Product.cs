﻿/**************************************************
* Created By: Sumit
* Created On: 4th feb 2016
* Modified By:
* Modified On:
* Purpose: To set the Product details.
**************************************************/

using System;
using System.Collections.Generic;
using System.Data;

namespace EntityModel
{
    public class Product
    {
        public string productName { get; set; }
        public float unitPrice { get; set; }
        public float unitWeight { get; set; }
        public int inStock { get; set; }
        public float discount { get; set; }
        public string description { get; set; }
        public int productId { get; set; }
        public int cartProductQuantity { get; set; }

        public static explicit operator Product(DataTable v)
        {
            throw new NotImplementedException();
        }

        public int usersId { get; set; }
        public string picture { get; set; }
        public List<ProductCategory> productCategories { get; set; }
        public object Rows { get; set; }
    }
}
