﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EntityModel
{
    public class UserMessage
    {
        public enum userMessage
        {
            exception = -1,
            failure = 0,
            success = 1
        }
    }
}
