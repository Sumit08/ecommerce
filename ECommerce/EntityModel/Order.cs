﻿/**************************************************
* Created By: Sumit
* Created On: 4th feb 2016
* Modified By:
* Modified On:
* Purpose: To set the Order details.
**************************************************/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EntityModel
{
    public class Order
    {
        public DateTime orderDate { get; set; }
        public string orderStatus { get; set; }
        public int orderId { get; set; }
        public int usersId { get; set; }
    }
}
