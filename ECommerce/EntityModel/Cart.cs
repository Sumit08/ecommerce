﻿/**************************************************
* Created By: Sumit
* Created On: 4th feb 2016
* Modified By:
* Modified On:
* Purpose: To set the Cart details.
**************************************************/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EntityModel
{
    public class Cart
    {
        public int cartId { get; set; }
        public int usersId { get; set; }
        public int productId { get; set; }
        public int productCount { get; set; }
    }
}
