﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EntityModel
{
    public class OrderDetail
    {
        public int ordersDetailId { get; set; }
        public int ordersId { get; set; }
        public int productId { get; set; }
        public int quantity { get; set; }
    }
}
