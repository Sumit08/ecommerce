﻿/**************************************************
* Created By: Sumit
* Created On: 20th feb 2016
* Modified By:
* Modified On:
* Purpose: To set the ProductCategory details.
**************************************************/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EntityModel
{
    public class ProductCategory
    {
        public int productCategoryId { get; set; }
        public string categoryName { get; set; }
        public List<Product> productList { get; set; }
        public string categoryImage { get; set; }
    }
}
