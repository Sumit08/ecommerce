﻿/**************************************************
* Created By: Sumit
* Created On: 4th feb 2016
* Modified By:
* Modified On:
* Purpose: To set the User details.
**************************************************/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EntityModel
{
    public class User
    {
        public string firstName { get; set; }
        public string lastName { get; set; }
        public string password { get; set; }
        public string mobileNumber { get; set; }
        public string email { get; set; }
        public int usersId { get; set; }
        public int roleId { get; set; }
    }
}
