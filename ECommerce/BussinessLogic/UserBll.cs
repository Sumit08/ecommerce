﻿/************************************************************************************
*Created By: Sumit
*Created On: 13 feb 2016
*Modified By:
*Modified On:
*Purpose: Calls the method of DataAccess layer and return value to Application layer.
**************************************************************************************/

using DataAccess;
using EntityModel;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;

namespace BussinessLogic
{
    public class UserBll
    {

        /// <summary>
        /// Purpose:Decodes the value and calls the insertUser method of DataAccess Layer. 
        /// </summary>
        public bool decodeUser(User user)
        {
            UserDetail userDetail = new UserDetail();
            byte[] temp = Convert.FromBase64String(user.email);
            user.email = Encoding.UTF8.GetString(temp);
            temp = Convert.FromBase64String(user.password);
            user.password = Encoding.UTF8.GetString(temp);
            return userDetail.insertUser(user);
        }

        /// <summary>
        /// Purpose:Decodes the value and calls the valiDate method of DataAccess Layer. 
        /// </summary>
        public static bool authenticate(User user)
        {
            byte[] temp = Convert.FromBase64String(user.email);
            user.email = Encoding.UTF8.GetString(temp);
            temp = Convert.FromBase64String(user.password);
            user.password = Encoding.UTF8.GetString(temp);
            int role = (int)userRole.user;
            return UserDetail.validateUser(user, role);
        }

        /// <summary>
        /// Purpose: Call saveOrderDll method of UserDetail class of DataAccess layer.
        /// </summary>
        /// <param name="address"></param>
        /// <param name="orderDetail"></param>
        /// <param name="order"></param>
        /// <returns></returns>
        public bool saveOrderBll(Address address, OrderDetail orderDetail, Order order)
        {
            UserDetail userDetail = new UserDetail();
            return userDetail.saveOrderDll(address, orderDetail, order);
        }

        /// <summary>
        /// Purpose: calls getOrder method of UserDetail class of DataAccess layer.
        /// </summary>
        /// <param name="usersId"></param>
        /// <returns></returns>
        public DataSet getOrder(int usersId)
        {
            UserDetail userDetail = new UserDetail();
            return userDetail.getOrder(usersId);
        }

        /// <summary>
        /// Purpose: calls getOrdersDetail method of UserDetail class of DataAccess layer.
        /// </summary>
        /// <param name="ordersId"></param>
        /// <returns></returns>
        public DataTable getOrdersDetail(int ordersId)
        {
            UserDetail userDetail = new UserDetail();
            return userDetail.getOrdersDetail(ordersId);
        }

        /// <summary>
        /// Purpose: calls addProductCart method of UserDetail class of DataAccess layer.
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="productId"></param>
        /// <returns></returns>
        public int addCart(int userId, int productId)
        {
            UserDetail userDetail = new UserDetail();
            return userDetail.addProductCart(userId, productId);
        }

        /// <summary>
        /// Purpose: Calls getCartDetail method of UserDetail class of DataAccess layer. 
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public DataTable cartProductDetail(int userId)
        {
            UserDetail userDetail = new UserDetail();
            return userDetail.getCartProduct(userId);
        }

        /// <summary>
        /// Purpose: Calls deleteCartProduct method of UserDetail class of DataAccess laye.
        /// </summary>
        /// <param name="productId"></param>
        /// <returns></returns>
        public bool deleteCartProduct(int productId)
        {
            UserDetail userDetail = new UserDetail();
            return userDetail.deleteCartProduct(productId);
        }

        /// <summary>
        /// Purpose: Calls getCartCount method of UserDetail class of DataAccess layer.
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public int getCartCount(int userId)
        {
            UserDetail userDtail = new UserDetail();
            return userDtail.getCartCount(userId);
        }

        ///// <summary>
        ///// Purpose: Call getCartDetail method of UserDetail class of DataAccess layer.
        ///// </summary>
        ///// <param name="userId"></param>
        ///// <returns></returns>
        //public List<Product> cartDetail(int userId)
        //{
        //    UserDetail userDetail = new UserDetail();
        //    return userDetail.getCartDetail(userId);
        //}

        /// <summary>
        /// Purpose: Calls insertCartOrder method of UserDetail class of DataAccess layer.
        /// </summary>
        /// <param name="address"></param>
        /// <param name="order"></param>
        /// <param name="list"></param>
        /// <returns></returns>
        public bool placeCartOrder(Address address, Order order, List<Product> productList)
        {
            string productIds = string.Empty;
            string inStock = string.Empty;
            string quantity = string.Empty;
            UserDetail userDetail = new UserDetail();
            foreach (Product product in productList)
            {
                productIds += (product.productId).ToString() + ",";
                inStock += (product.inStock - product.cartProductQuantity).ToString() + ",";
                quantity += (product.cartProductQuantity).ToString() + ",";
            }
            productIds = productIds.Remove(productIds.Length - 1);
            inStock = inStock.Remove(inStock.Length - 1);
            quantity = quantity.Remove(quantity.Length - 1);

            return userDetail.insertCartOrder(address, order, productIds, inStock, quantity);
        }

        /// <summary>
        /// Purpose: Calls updateUser method of UserDetail class of DataAccess layer.
        /// </summary>
        /// <param name="address"></param>
        /// <param name="user"></param>
        /// <returns></returns>
        public bool editProfile(Address address, User user)
        {
            UserDetail userDetail = new UserDetail();
            return userDetail.updateUser(address, user);
        }

        /// <summary>
        /// Purpose: Calls fetchPassword method of UserDetail class of DataAccess layer.
        /// </summary>
        /// <param name="email"></param>
        /// <returns></returns>
        public string getPassword(string email)
        {
            UserDetail userDetail = new UserDetail();
            return userDetail.fetchPassword(email);
        }

        /// <summary>
        /// Purpose: Calls getProductList method of UserDetail class of DataAccess layer.
        /// </summary>
        /// <param name="categoryId"></param>
        /// <returns></returns>
        public DataTable getProductList(int categoryId)
        {
            UserDetail userDetail = new UserDetail();
            return userDetail.getProductList(categoryId);
        }

        /// <summary>
        /// Purpose: Calls listCategory method of UserDetail class of DataAccess layer.
        /// </summary>
        /// <returns></returns>
        public List<ProductCategory> listCategory()
        {
            UserDetail userDetail = new UserDetail();
            return userDetail.listCategory();
        }

        /// <summary>
        /// Purpose: Calls searchItem method of UserDetail class pf DataAccess layer.
        /// </summary>
        /// <param name="searchWord"></param>
        /// <returns></returns>
        public List<Product> searchItem(string searchWord)
        {
            UserDetail userdetail = new UserDetail();
            return userdetail.searchItem(searchWord);
        }

        /// <summary>
        /// Purpose: update the quantity of product in cart.
        /// </summary>
        /// <param name="produtId"></param>
        /// <param name="quantity"></param>
        /// <param name="userId"></param>
        /// <returns></returns>
        public bool updateCartQuantity(int produtId, int quantity, int userId)
        {
            UserDetail userDetail = new UserDetail();
            return userDetail.updateCartQuantity(produtId, quantity, userId);
        }

    }
}
