﻿/************************************************************************************************************
*Created By: Sumit
*Created On: 14 feb 2016
*Modified By:
*Modified On:
*Purpose: Calls the method of DataAcceess layer related to supplier and return value to the Application layer.
**************************************************************************************************************/

using DataAccess;
using EntityModel;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Text;

namespace BussinessLogic
{
    public class SuplierBll
    {
        public User user { get; set; }
        public Address address { get; set; }

        /// <summary>
        /// Purpose: Constructor being called on the object registerSupplier creation in application layer.
        /// </summary>
        public SuplierBll()
        {
            user = this.user;
            address = this.address;
        }

        /// <summary>
        /// Purpose: calling saveSupplier method form DataAccess layer and accepts user Id, depending on user Id calls the saveAddress method of DataAccess layer.
        /// </summary>
        public int InsertSupplier(SuplierBll reg)
        {
            SupplierDetail supplierDetail = new SupplierDetail();
            int userId = supplierDetail.saveSupplier(user);
            reg.address.usersId = userId;
            if (userId > 0)
            {
                return supplierDetail.saveAddress(address);
            }
            else
            {
                return 0;
            }
        }

        /// <summary>
        /// Purpose: Decode email and password of supplier and calls validateUser method from DataAccess layer
        /// </summary>
        public static bool authenticateSupplier(User user)
        {

            byte[] temp = Convert.FromBase64String(user.email);
            user.email = Encoding.UTF8.GetString(temp);

            temp = Convert.FromBase64String(user.password);
            user.password = Encoding.UTF8.GetString(temp);
            int role = (int)userRole.supplier;
            return UserDetail.validateUser(user, role);
        }

        /// <summary>
        /// Purpose: Calling getProductCategory method from DataAccess layer and returns entity type list.
        /// </summary>
        public List<ProductCategory> getCategory()
        {
            SupplierDetail supplierDetail = new SupplierDetail();
            return supplierDetail.getProductCategory();
        }

        /// <summary>
        /// Purpose: Calling getProductName method from DataAccess layer and returns entity type list.
        /// </summary>
        public List<Product> getProdut(int UserId)
        {
            SupplierDetail supplierDetail = new SupplierDetail();
            return supplierDetail.getProductName(UserId);
        }

        /// <summary>
        /// Purpose: calling DataAccess layer insert product method and creating DataTable to store categoryId and passing it .
        /// </summary>
        public int addProduct(Product product)
        {
            SupplierDetail supplierDetail = new SupplierDetail();
            DataTable CategoryTable = new DataTable();
            CategoryTable.Columns.Add(new DataColumn("productCategoryId"));
            foreach (var productCategory in product.productCategories)
            {
                CategoryTable.Rows.Add(productCategory.productCategoryId);
            }
            return supplierDetail.insertProduct(product, CategoryTable);
        }

        /// <summary>
        /// Purpose: Calling DataAccess layer bindProduct fields returning Product object.
        /// </summary>
        public Product bindProductFields(int ProductId)
        {
            SupplierDetail supplierDetail = new SupplierDetail();
            return supplierDetail.getProductDetail(ProductId);
        }

        /// <summary>
        /// Purpose:Calling DataAccess layer updateProductDetail and creating DataTable and passing it.
        /// </summary>
        public bool updateProduct(Product product)//, List<ProductCategory> productCategory)
        {
            SupplierDetail suplierDetail = new SupplierDetail();
            DataTable CategoryTable = new DataTable();
            CategoryTable.Columns.Add(new DataColumn("productCategoryId"));
            foreach (var productCategory in product.productCategories)
            {
                CategoryTable.Rows.Add(productCategory.productCategoryId);
            }
            return suplierDetail.updateProductDetail(product, CategoryTable);
        }

        /// <summary>
        /// Purpose: Generating random id for product category.
        /// </summary>
        /// <returns></returns>
        public List<int> randomSelect()
        {
            List<int> randomNumber = new List<int>();
            Random random = new Random();
            int number;
            while (randomNumber.Count != 3)
            {
                number = random.Next(1, 4);
                if (!randomNumber.Contains(number))
                {
                    randomNumber.Add(number);
                }

            }
            return randomNumber;
        }

        /// <summary>
        /// Purpose: getProductDetailByCategory method of DataAccess layer is called.
        /// </summary>
        /// <returns></returns>
        public List<ProductCategory> getProductByCategory()
        {
            SupplierDetail suplierDetail = new SupplierDetail();
            return suplierDetail.getProductDetailByCategory();
        }

        /// <summary>
        /// Purpose: Calls fetchCountries method of SupplierDetail class of DataAccess Layer.
        /// </summary>
        /// <returns></returns>
        public Dictionary<int, string> getCountry()
        {
            SupplierDetail supplierDetail = new SupplierDetail();
            DataTable countryTable = supplierDetail.fetchCountries();
            var countries = new Dictionary<int, string>();
            foreach (var row in countryTable.AsEnumerable())
            {
                countries.Add((int)row["CountryId"], (string)row["CountryName"]);
            }
            return countries;
        }

        /// <summary>
        /// Purpose: Calls fetchStates method of SupplierDetail class of DataAccess Layer.
        /// </summary>
        /// <param name="countryId"></param>
        /// <returns></returns>
        public Dictionary<int, string> getState(int countryId)
        {
            SupplierDetail supplierDetail = new SupplierDetail();
            DataTable stateTable = supplierDetail.fetchStates(countryId);
            var states = new Dictionary<int, string>();
            foreach (var row in stateTable.AsEnumerable())
            {
                states.Add((int)row["StateId"], (string)row["StateName"]);
            }
            return states;
        }
    }
}
