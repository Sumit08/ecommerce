﻿using DataAccess;
using EntityModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BussinessLogic
{
    public class SessionValue
    {
        public string addresses { get; set; }
        public string city{ get; set; }
        public string postalCode { get; set; }
        public string firstName { get; set; }
        public string lastName { get; set; }
        public string mobileNumber { get; set; }
        public string email { get; set; }
        public int userId { get; set; }
        public int productCount { get; set; }

        public SessionValue getUserDetail(string emailId)
        {
            SessionValue sessionValue = new SessionValue();
            UserDetail userDetail = new UserDetail();
            var user = userDetail.getUserDetail(emailId);
            var address = userDetail.getUserAddress(user.usersId);
            sessionValue.addresses = address.addresses;
            sessionValue.city = address.city;
            sessionValue.postalCode = address.postalCode;
            sessionValue.mobileNumber = user.mobileNumber;
            sessionValue.firstName = user.firstName;
            sessionValue.lastName = user.lastName;
            sessionValue.email = user.email;
            sessionValue.userId = user.usersId;
            sessionValue.productCount = userDetail.getCartCount(user.usersId);
            return sessionValue;
        }
    }
}
